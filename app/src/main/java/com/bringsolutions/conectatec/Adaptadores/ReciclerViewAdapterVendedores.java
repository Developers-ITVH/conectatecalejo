package com.bringsolutions.conectatec.Adaptadores;

import android.app.Dialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bringsolutions.conectatec.BD.SQLHelper;
import com.bringsolutions.conectatec.R;
import com.bringsolutions.conectatec.objetos.Vendedor;
import com.bumptech.glide.Glide;

import java.util.List;

public class ReciclerViewAdapterVendedores extends RecyclerView.Adapter<ReciclerViewAdapterVendedores.ViewHolder>{


    //clase viewholder para enlazar componesntes con la vista de los mensajes
    public static class ViewHolder extends RecyclerView.ViewHolder{

        private TextView tipoVendedor,descripcion,hora,usuario;
        private CardView complementaria;

         ImageView imagen1,imagen2,imagen3;
        private de.hdodenhof.circleimageview.CircleImageView fotovendedor;
        private Button what,llamada;

        //titulos
        TextView tituloNombre, tituloHorario, tituloDescripcion, tituloProductosOfrecidos;


        public ViewHolder(View itemView) {
            super(itemView);
            //enalanzando elementos

            usuario = itemView.findViewById(R.id.tvNombreVendedorTarjeta);
            hora = itemView.findViewById(R.id.tvHorarioVendedorTarjeta);
            descripcion = itemView.findViewById(R.id.tvDescripcionVendedorTarjeta);
            what = itemView.findViewById(R.id.btnWhatsTarjetaVendedor);
            llamada = itemView.findViewById(R.id.btnLlamarTarjetaVendedor);
            imagen1 = itemView.findViewById(R.id.foto1TarjetaVendedor);
            imagen2 = itemView.findViewById(R.id.foto2TarjetaVendedor);
            imagen3 = itemView.findViewById(R.id.foto3TarjetaVendedor);
            fotovendedor = itemView.findViewById(R.id.fotoTarjetaVendedor);
            complementaria = itemView.findViewById(R.id.tarjetita);


            tituloNombre = itemView.findViewById(R.id.tNombre);
            tituloHorario= itemView.findViewById(R.id.tHorario);
            tituloDescripcion= itemView.findViewById(R.id.tDescripcion);
            tituloProductosOfrecidos = itemView.findViewById(R.id.tProductosOfre);


        }
    }


    public List<Vendedor> vendedoreslista;
    public Context context;

    public ReciclerViewAdapterVendedores(List<Vendedor> vendedoreslista, Context context) {
        this.vendedoreslista = vendedoreslista;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarjeta_vendedor,viewGroup,false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final  int i) {
        viewHolder.usuario.setText(vendedoreslista.get(i).getNombreUsuario());
        viewHolder.hora.setText( "DE  " +vendedoreslista.get(i).getHoraIncio()+ " "+vendedoreslista.get(i).getTiempoInicio() +"  A  " +vendedoreslista.get(i).getHoraFinal()+" "+vendedoreslista.get(i).getTiempoFinal());
        viewHolder.descripcion.setText(vendedoreslista.get(i).getDescripcion());

        Glide.with(context).load(Uri.parse(vendedoreslista.get(i).getFotoUno())).into(viewHolder.imagen1);
        Glide.with(context).load(Uri.parse(vendedoreslista.get(i).getFotoDos())).into(viewHolder.imagen2);
        Glide.with(context).load(Uri.parse(vendedoreslista.get(i).getFotoTres())).into(viewHolder.imagen3);
        Glide.with(context).load(Uri.parse(vendedoreslista.get(i).getFotoPerfilUsuario())).into(viewHolder.fotovendedor);


        //CREO UN INSTANACIA DE LA BASE DE DATOS
        SQLHelper conn = new SQLHelper(context,SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);

        //LA ESTABLESCO EN MODO LECTURA
        SQLiteDatabase db=conn.getReadableDatabase();

        //CREO UN CURSOR PARA RECORRER LOS CAMPOS
        Cursor cursor = db.rawQuery("SELECT estado from tema;", null);

        //CREO VARIABLES PARA GUARDAR LO QUE ME RETORNE EL CURSOR SEGÚN EL TIPO DE VALOR O DATO
        String estado="";

        while(cursor.moveToNext()){

            //GUARDO LOS RESULTADOS EN LAS VARIABLES
            estado  = cursor.getString(0);

            if(estado.equals("true")){
                //TEMA BLACK

                viewHolder.complementaria.setCardBackgroundColor(Color.parseColor("#1F1E1E"));
                viewHolder.tituloNombre.setTextColor(Color.parseColor("#FFFFFF"));
                viewHolder.tituloHorario.setTextColor(Color.parseColor("#FFFFFF"));
                viewHolder.tituloDescripcion.setTextColor(Color.parseColor("#FFFFFF"));
                viewHolder.tituloProductosOfrecidos.setTextColor(Color.parseColor("#FFFFFF"));
                viewHolder.usuario.setTextColor(Color.parseColor("#FFDC00"));
            }else{
                //VALORES ORIGINALES
                viewHolder.complementaria.setCardBackgroundColor(Color.parseColor("#ffffff"));


            }


        }



        viewHolder.imagen1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Service.LAYOUT_INFLATER_SERVICE);

                final Dialog builder = new Dialog(context);
                builder.requestWindowFeature(Window.FEATURE_NO_TITLE);

                View dialoglayout = inflater.inflate(R.layout.imagen_dialog, null);
                ImageView imagenInflada = dialoglayout.findViewById(R.id.imagenDialog);

                builder.setContentView(dialoglayout);

                Glide.with(context).load(Uri.parse(vendedoreslista.get(i).getFotoUno())).into(imagenInflada);

                builder.show();


            }
        });

        viewHolder.imagen2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Service.LAYOUT_INFLATER_SERVICE);

                final Dialog builder = new Dialog(context);
                builder.requestWindowFeature(Window.FEATURE_NO_TITLE);

                View dialoglayout = inflater.inflate(R.layout.imagen_dialog, null);
                ImageView imagenInflada = dialoglayout.findViewById(R.id.imagenDialog);

                builder.setContentView(dialoglayout);

                Glide.with(context).load(Uri.parse(vendedoreslista.get(i).getFotoDos())).into(imagenInflada);

                builder.show();


            }
        });

        viewHolder.imagen3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Service.LAYOUT_INFLATER_SERVICE);

                final Dialog builder = new Dialog(context);
                builder.requestWindowFeature(Window.FEATURE_NO_TITLE);

                View dialoglayout = inflater.inflate(R.layout.imagen_dialog, null);
                ImageView imagenInflada = dialoglayout.findViewById(R.id.imagenDialog);

                builder.setContentView(dialoglayout);

                Glide.with(context).load(Uri.parse(vendedoreslista.get(i).getFotoTres())).into(imagenInflada);

                builder.show();


            }
        });


        viewHolder.llamada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+vendedoreslista.get(i).getTelefonoUsuario())));
            }
        });
        viewHolder.what.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("https://api.whatsapp.com/send?phone=52"+vendedoreslista.get(i).getTelefonoUsuario()+"&text=Hola "+vendedoreslista.get(i).getNombreUsuario()+"! Uno de tus productos me resultó interesante.");
                context.startActivity( new Intent(Intent.ACTION_VIEW, uri));
                Toast.makeText(context, "Se abrirá en WhatsApp", Toast.LENGTH_SHORT).show();
            }
        });


    }




    @Override
    public int getItemCount() {
        return vendedoreslista.size();
    }


}
