package com.bringsolutions.conectatec.formularios;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bringsolutions.conectatec.BD.SQLHelper;
import com.bringsolutions.conectatec.MainActivity;
import com.bringsolutions.conectatec.R;
import com.bringsolutions.conectatec.objetos.CONSTANTES;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Timer;
import java.util.TimerTask;

import static com.bringsolutions.conectatec.objetos.CONSTANTES.USUARIO;

public class InicioSesion extends AppCompatActivity {
    boolean respuestaExistenciaUsuario;

    //ELEMENTOS PARA EL INICIO DE SESIÓN
    EditText cajaCorreo, cajaPass;
    Button btnInciarSesion;
    TextView textoRegistrarse;
    CheckBox mantenerSesion;

    //ELEMENTOS PARA VOLLEY
    RequestQueue requestQueue;
    JsonObjectRequest jsonObjectRequest;

    //ANIMACIÓN
    LottieAnimationView animacionLottie;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_sesion);
       this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

       final TextView titulo = findViewById(R.id.titleContectaTec);
       final  String[] frases = new String[4];

        frases[0] = "BringSolutions";
        frases[1] = "Contáctanos!";
        frases[2] = "Desarrollo de Software";
        frases[3] = "ConectaTec";

        final Handler handler = new Handler();

        Timer timer = new Timer();

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            int numFrase = (int) (Math.random() * frases.length);

                            titulo.setText(frases[numFrase]);

                         //   Toast.makeText(InicioSesion.this, "Hola je", Toast.LENGTH_SHORT).show();
                        }catch (Exception e){

                        }
                    }
                });
            }
        };
        timer.schedule(task,0,2000);


        //LLAMO LOS MÉTODOS
        iniciarlizarElementos();
        clicksBotones();

        //CREO INSTANCIA A MI BASE DATOS
        SQLHelper conn = new SQLHelper(getApplicationContext(),"recordar", null, 1);
        //LA ESTABLESCO EN MODO LECTURA
        SQLiteDatabase db=conn.getReadableDatabase();

        //CREO UN CURSOR PARA RECORRER LOS CAMPOS
        Cursor cursor = db.rawQuery("SELECT correo, pass from recordarpass;", null);

        //CREO VARIABLES PARA GUARDAR LO QUE ME RETORNE EL CURSOR SEGÚN EL TIPO DE VALOR O DATO
        String correo="", pass="";

        while(cursor.moveToNext()){

            //GUARDO LOS RESULTADOS EN LAS VARIABLES
            correo  = cursor.getString(0);
            pass  = cursor.getString(1);


            //SI LAS VARIABLES CONTIENEN VALORES ESPECÍFICOS QUIERE DECIR QUE EL USUARIO NO SELECCIONÓ
            //LA CASILLA GUARDAR DATOS
            if (correo.equals("sincorreo") && pass.equals("sinpass")){
                //SETEO EN LOS CAMPOS ""
                cajaPass.setText("");
                cajaCorreo.setText("");
            }else{
                //EN CASO DE QUE EL CONTENIDO DE LAS VARIABLES NO SEAN LOS ANTERIORES, SETEO EN LOS CAMPOS
                //LO OBTENIDO QUE POR ENDE SERÁ UN CORREO Y PASS
                    cajaPass.setText(pass);
                    cajaCorreo.setText(correo);
                    //HABILITO LA CASILLA  MANTENER DATOS
                    mantenerSesion.setChecked(true);
                    //VERIFICO SI ESE USUARIO EXISTE, ESTE MÉTODO AUTOMÁTICANTE ME LLEVA A LA ACTIVIDAD PRINCIPAL
                    //SI LOS DATOS SON CORRECTOS.
                    confirmarExistenciaUsuarioWebService(correo,pass);

            }

        }
    }

    //CLICKS A LOS BOTONES INICIAR SESIÓN Y RECORDAR DATOS
    void clicksBotones(){

        btnInciarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnInciarSesion.setEnabled(false);

                if(cajaCorreo.getText().toString().isEmpty() || cajaPass.getText().toString().isEmpty()){
                        Toast.makeText(InicioSesion.this, "Algún campo sin rellenar.", Toast.LENGTH_LONG).show();

                    }else {

                        prepaparDatos(cajaCorreo.getText().toString(), cajaPass.getText().toString());
                    }
            }
        });

        textoRegistrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(InicioSesion.this, Registro.class));


            }
        });

    }

    //CASTINGS DE TODOS LOS ELEMENTOS A USAR
    void iniciarlizarElementos(){
        cajaCorreo = findViewById(R.id.txtCorreo);
        cajaPass = findViewById(R.id.txtPass);
        btnInciarSesion = findViewById(R.id.btnIniciarSesion);
        mantenerSesion  = findViewById(R.id.mantenerSesion);
        textoRegistrarse = findViewById(R.id.textoBoton);
        requestQueue = Volley.newRequestQueue(this);
        animacionLottie =findViewById(R.id.lottieInicioSesion);

        Shader myShader = new LinearGradient(
                0, 0, 2, 100,
                Color.parseColor("#1d9e00"), Color.YELLOW,
                Shader.TileMode.CLAMP );
        textoRegistrarse.getPaint().setShader( myShader );

    }
    //PREPARO LOS DATOS Y REALIZO VALIDACIONES CON LOS ELEMENTOS DEL LOGIN
    private void prepaparDatos(String correoUsu, String  passUsu) {
        //CREO UN INSTANACIA DE LA BASE DE DATOS
        SQLHelper conn = new SQLHelper(getApplicationContext(),SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);
        //LA ABRO EN MODO ESCRITURA
        SQLiteDatabase db=conn.getWritableDatabase();

        //SI LA CASILLA RECORDAR DATOS FUE SELECCIONADA
        if(recordarDatos()){
            // ACTUALIZO LOS CAMPOS CON LOS NUEVOS DATOS DE LOS EDITTEX
            db.execSQL("UPDATE recordarpass SET estado='true', correo= '"+correoUsu+"', pass='"+passUsu+"';");
           boolean res= confirmarExistenciaUsuarioWebService(correoUsu, passUsu);

            //EN CASO DE QUE NO EXISTA EL USUARIO
           if(res=false){
               //VUELVO A LOS VALORES ORIGINALES PARA CADA CAMPO
               db.execSQL("UPDATE recordarpass SET estado='false',correo ='sincorreo', pass='sinpass';");

           }
        }else{

            //EN EL CASO DE QUE LA CASILLA NO HAYA SIDO SELECCIONADA, MANTENGO LOS DATOS SIN VALORES EN LA TABLA
            db.execSQL("UPDATE recordarpass SET estado='false',correo ='sincorreo', pass='sinpass';");

            //VERIFICO LA EXISTENCIA DEL USUARIO
           confirmarExistenciaUsuarioWebService(correoUsu, passUsu);

        }


    }

    //RETORNA TRUE SI EL USUARIO EXISTE Y NOS LLEVA A LA ACTIVIDAD PRINCIPAL
    //EN CASO DE FALSO, MANDA UN TOAST QUE EL USUARIO NO EXISTE
    public boolean confirmarExistenciaUsuarioWebService(String correo, String pass){

        String url =    CONSTANTES.URL_PRINCIPAL+"buscar_u.php?correo="+correo+"&pass="+pass;

        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                JSONArray json = response.optJSONArray("usuario");
                JSONObject jsonObject = null;

                try {
                    //CARGO UN OBJETO DE TIPO USUARIO Y SETEO LOS DATOS CONSUMIDOS POR EL WEBSERVICE
                    jsonObject=json.getJSONObject(0);
                    USUARIO.setIdusuario(jsonObject.optString("idusuario"));
                    USUARIO.setNombre(jsonObject.optString("nombre"));
                    USUARIO.setCorreo(jsonObject.optString("correo"));
                    USUARIO.setCarrera(jsonObject.optString("carrera"));
                    USUARIO.setTipo(jsonObject.optString("tipo"));
                    USUARIO.setPass(jsonObject.optString("pass"));
                    USUARIO.setTelefono(jsonObject.optString("telefono"));


                    respuestaExistenciaUsuario=true;
                    //CARGO LA ANIMACIÓN
                    animacionLottie.setAnimation("confetti.json");
                    animacionLottie.playAnimation();

                    //ESPERO UN SEGUNDO Y MEDIO Y MANDO A LA ACTIVIDAD PRINCIPAL
                    Handler handler = new Handler();

                    handler.postDelayed(new Runnable() {
                        public void run() {
                            finish();
                            startActivity(new Intent(InicioSesion.this,MainActivity.class));
                        }
                    }, 1500);

                    //MANDO UN MENSAJE DE BIENVENIDA SI SE ENCONTRÓ EL USUARIO CONCATENANDO SU NOMBRE DEL OBJETO
                    Toast.makeText(getApplicationContext(),"¡Hola "+USUARIO.getNombre()+"!", Toast.LENGTH_SHORT).show();

                } catch (JSONException e) {
                    btnInciarSesion.setEnabled(true);
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //EN CASO DE QUE NO EXISTA
                respuestaExistenciaUsuario=false;
                Toast.makeText(getApplicationContext(),"El usuario no existe o los datos no son correctos.", Toast.LENGTH_SHORT).show();
                btnInciarSesion.setEnabled(true);
            }
        });
        requestQueue.add(jsonObjectRequest);

        //RETORNO EL VALOR FINAL
        return  respuestaExistenciaUsuario;

    }

    //RETORNA VERDADERO O FALSO SI LA CASILLA RECORDAR DATOS ESTÁ SELECCIONADA O NO
    public boolean recordarDatos(){

        return mantenerSesion.isChecked();
    }
}
