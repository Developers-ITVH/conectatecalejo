package com.bringsolutions.conectatec.formularios;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bringsolutions.conectatec.R;
import com.bringsolutions.conectatec.objetos.CONSTANTES;

import org.json.JSONObject;


public class Registro extends AppCompatActivity{
    boolean respuestaExistenciaUsuario;

    String nombre, correo, telefono;

    boolean comprobacionPass;
    String passFinal;

    EditText txtNombre, txtCorreo, txtTelefono, txtxPass, txtComfirmPass;

    RadioGroup grupoTipoUsuario;
    RadioButton estudiante, vendedor;
    Button registrar;

    RequestQueue requestQueue;

    StringRequest stringRequest;


    ProgressDialog progreso;
    LottieAnimationView animacionLottie;

    //ELEMENTOS PARA VOLLEY

    JsonObjectRequest jsonObjectRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        inicializar();

        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrar.setEnabled(false);
                nombre= txtNombre.getText().toString();
                correo = txtCorreo .getText().toString();
                telefono= txtTelefono .getText().toString();
                comprobacionPass= comprobarPass(txtxPass.getText().toString(), txtComfirmPass.getText().toString());

                if(comprobacionPass){

                    if(validacionCorreo(correo)==true){

                        if(validacionTelefono(telefono)==true){
                            confirmarExistenciaUsuarioWebServiceXcorreo(correo);

                        }else{
                            Toast.makeText(getApplicationContext(), "Teléfono inválido.", Toast.LENGTH_SHORT).show();
                            registrar.setEnabled(true);
                        }
                    }else{
                        registrar.setEnabled(true);
                        Toast.makeText(getApplicationContext(), "Correo electrónico inválido.", Toast.LENGTH_SHORT).show();
                    }

                }else{
                    registrar.setEnabled(true);
                    Toast.makeText(getApplicationContext(), "Las contraseñas no coinciden o es muy corta.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void cargarWebService() {
        progreso = new ProgressDialog(this);
        progreso.setMessage("Cargando...");
        progreso.show();
        
        passFinal=txtxPass.getText().toString();
        
        String url =     CONSTANTES.URL_PRINCIPAL+"insertar_u.php?nombre="+nombre+"&correo="+correo+"&carrera=Sistemas&tipo="+String.valueOf(tipoUsuario())+"&pass="+passFinal+"&telefono="+telefono+"&numreportes=0";

        registrarVendedor("","","","","","","","","","","","","");

        url=url.replace(" ", "%20");

        stringRequest= new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.toast_personalizado,(ViewGroup) findViewById(R.id.toastLayout));

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setView(layout);
                    toast.show();

                    progreso.hide();

                    animacionLottie.setAnimation("check.json");
                    animacionLottie.playAnimation();

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            finish();
                            startActivity(new Intent(Registro.this, InicioSesion.class));


                        }
                    }, 2000);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), "Se produjo un problema a la hora de realizar el registro. "+error.toString(), Toast.LENGTH_LONG).show();
                    progreso.hide();
                }
            });

            requestQueue.add(stringRequest);
    }

    public void  registrarVendedor(String h_inicio, String tiempo_inicio, String h_final, String tiempo_final, String tipo_vendedor, String descripcion, String foto_uno, String foto_dos, String foto_tres, String correo, String fotoPerfilUsuario, String usuarioNombre, String telefonoUsuario){
        
        String url = "http://35.231.226.21/insertar_v.php?hora_inicio="+h_inicio+"&t_inicio="+tiempo_inicio+"&hora_final="+h_final+"&t_final="+tiempo_final+"&tipo_vendedor="+tipo_vendedor+"&descripcion="+descripcion+"&foto_uno="+(foto_uno.replace("%", "%25").replace("&", "%26"))+"&foto_dos="+(foto_dos.replace("%", "%25").replace("&", "%26"))+"&foto_tres="+(foto_tres.replace("%", "%25").replace("&", "%26"))+"&correo="+correo+"&foto_perfil="+(fotoPerfilUsuario.replace("%", "%25").replace("&", "%26"))+"&usuario_nombre="+usuarioNombre+"&telefono_usuario="+telefonoUsuario;

        stringRequest= new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
            

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            
            }
        });

        requestQueue.add(stringRequest);

    }


    //RETORNA TRUE SI EL USUARIO EXISTE Y NOS LLEVA A LA ACTIVIDAD PRINCIPAL
    //EN CASO DE FALSO, MANDA UN TOAST QUE EL USUARIO NO EXISTE
    public boolean confirmarExistenciaUsuarioWebServiceXcorreo(String correo){

        String url ="http://35.231.226.21/buscar_u_xcorreo.php?correo="+correo;

        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                respuestaExistenciaUsuario=true;
                Toast.makeText(getApplicationContext(),"Ya existe un usuario con ese correo.", Toast.LENGTH_SHORT).show();
                registrar.setEnabled(true);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                respuestaExistenciaUsuario=false;
                cargarWebService();
                registrar.setEnabled(true);
            }
        });
        
        requestQueue.add(jsonObjectRequest);
        //RETORNO EL VALOR FINAL
        return  respuestaExistenciaUsuario;
    }

    //COMPRUEBA QUE SEAN IGUALES Y QUE LA LOGINTUD SEA MAYOR A 6
    boolean comprobarPass(String pass, String pass2){
        boolean respuesta;

        if( pass.equals(pass2) && pass.length()>6 ){
            respuesta=true;
        }else{
            respuesta=false;
        }
        
        
        return respuesta;

    }

    //CASTING A TODOS LOS COMPONENTES DEL REGISTRO
    void inicializar(){

        txtNombre = findViewById(R.id.nombre);
        txtCorreo = findViewById(R.id.correo);
        txtTelefono = findViewById(R.id.telefono);
        txtxPass = findViewById(R.id.pass);
        txtComfirmPass = findViewById(R.id.pass2);
        grupoTipoUsuario = findViewById(R.id.grupoRadioButtons);
        estudiante = findViewById(R.id.estudianteRadio);
        vendedor = findViewById(R.id.vendedorRadio);
        registrar = findViewById(R.id.btnRegistrarse);

        requestQueue = Volley.newRequestQueue(this);

        animacionLottie = findViewById(R.id.lottieRegistro);

        estudiante.setChecked(true);

    }

    //COMPRUEBA SI EL VENDEDOR O ESTUDIANTE
    int tipoUsuario(){
        int res=0;
        if(estudiante.isChecked()==true){
            res=1;

        }else if(vendedor.isChecked()==true){

            res=2;
        }
        return  res;
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }


    boolean validacionCorreo (String correo){
        return correo.contains("@");
    }

    boolean validacionTelefono(String tel){

        boolean res;

        if(tel.length()>10 || telefono.length()<9){
            res=false;
        }else{

            res=true;
        }
        return res;
    }



}
