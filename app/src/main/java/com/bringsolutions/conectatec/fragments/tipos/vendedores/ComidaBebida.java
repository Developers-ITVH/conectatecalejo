package com.bringsolutions.conectatec.fragments.tipos.vendedores;


import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bringsolutions.conectatec.Adaptadores.ReciclerViewAdapterVendedores;
import com.bringsolutions.conectatec.R;
import com.bringsolutions.conectatec.objetos.CONSTANTES;
import com.bringsolutions.conectatec.objetos.Vendedor;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ComidaBebida extends Fragment {

    View  view;
    private RecyclerView recyclerView;
    private ReciclerViewAdapterVendedores adapter;
    List<Vendedor> VENDEDORES = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view=  inflater.inflate(R.layout.fragment_comida_bebida, container, false);

        recyclerView = view.findViewById(R.id.recyclerComidaBebida);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        llenarRecyclerVendedores("Comidas o Bebidas");


    return  view;
    }

    private void llenarRecyclerVendedores(String categoria) {
        //Crear un objeto requetqueue
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        //Url del webservice
        String url =     CONSTANTES.URL_PRINCIPAL+ "buscar_vendedor_xtipo.php?tipo="+categoria;

        // crear un objeto requet
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                JSONArray json = response.optJSONArray("vendedor");
                JSONObject jsonObject = null;

                try {

                    for (int i =0 ; i<json.length();i++){
                        jsonObject = json.getJSONObject(i);

                        String horaInicio= jsonObject.optString("hora_inicio");
                        String tiempoInicio= jsonObject.optString("t_incio");
                        String horaFinal= jsonObject.optString("hora_final");
                        String tiempoFinal= jsonObject.optString("t_final");
                        String descripcion= jsonObject.optString("descripcion");
                        String foto1= jsonObject.optString("foto_uno");
                        String foto2= jsonObject.optString("foto_dos");
                        String foto3= jsonObject.optString("foto_tres");
                        String fotoVendedor = jsonObject.optString("foto_perfil");
                        String nombreVendedor = jsonObject.optString("usuario_nombre");
                        String telefonoVendedor = jsonObject.optString("telefono_usuario");



                        VENDEDORES.add(new Vendedor(horaInicio,tiempoInicio,horaFinal,tiempoFinal,descripcion,foto1,foto2,foto3,fotoVendedor,nombreVendedor,telefonoVendedor));


                        adapter = new ReciclerViewAdapterVendedores(VENDEDORES,getActivity());
                        recyclerView.setAdapter(adapter);

                    }


                } catch (Exception e) {
                    Toast.makeText(getActivity(), "No hay vendedores en esta categoría", Toast.LENGTH_SHORT).show();

                }
                // si hay respuesta


            }
        }, new Response.ErrorListener() {
            @Override


            public void onErrorResponse(VolleyError error) {

                //error de respuesta
                Toast.makeText(getActivity(), "No hay vendedores en esta categoría", Toast.LENGTH_SHORT).show();

            }
        });

        //añadir el objeto requet al requetqueue
        requestQueue.add(jsonObjectRequest);

    }

}
