package com.bringsolutions.conectatec.tipos.locatec;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bringsolutions.conectatec.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CroquisTec extends Fragment {


    public CroquisTec() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_croquis_tec, container, false);
    }

}
