package com.bringsolutions.conectatec.tipos.locatec;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bringsolutions.conectatec.BD.SQLHelper;
import com.bringsolutions.conectatec.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

public class MapaConectaTec extends Fragment  {
	View view;
	MapView mapView;
	public final static double RADIO_DE_LA_TIERRA_EN_KM = 6371;
	private static int PETICION_PERMISO_LOCALIZACION = 101;
	Spinner spnTipoLugar, spnNombreLugar;
	String seleccionTipo;
	
	private GoogleMap mMap;
	private Marker marcador;
	double lat = 0.0;
	double lng = 0.0;
	String mensaje1;
	String direccion = "";
	
	double latiC=18.024098, longC=-92.90337;
	double latiB=18.024088, longB=-92.90337;
	double latiA=18.024113, longA=-92.903478;
	double latiE=18.02362,  longE=-92.904317;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_mapa_conecta_tec, container, false);
		
		spnTipoLugar= view.findViewById(R.id.spinnerTipoUbicación);
		spnNombreLugar = view.findViewById(R.id.spinnerNombreUbicacion);
		
		
		examinarTema();
		
		
		spnTipoLugar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				ArrayAdapter<CharSequence> adaptador=null;
				
				String tipoLugar = spnTipoLugar.getSelectedItem().toString();
				
				if(tipoLugar.equals("EDIFICIO")){
					adaptador= ArrayAdapter.createFromResource(getActivity(), R.array.array_edificios, android.R.layout.simple_spinner_dropdown_item);
					seleccionTipo="EDIFICIO";
				}else if(tipoLugar.equals("DEPARTAMENTO")){
					adaptador= ArrayAdapter.createFromResource(getActivity(), R.array.array_departamentos, android.R.layout.simple_spinner_dropdown_item);
					seleccionTipo="DEPARTAMENTO";
				}else if(tipoLugar.equals("LABORATORIO")){
					adaptador= ArrayAdapter.createFromResource(getActivity(), R.array.array_laboratorios, android.R.layout.simple_spinner_dropdown_item);
					seleccionTipo="LABORATORIO";
				}else if(tipoLugar.equals("BAÑOS")){
					adaptador= ArrayAdapter.createFromResource(getActivity(), R.array.array_wc, android.R.layout.simple_spinner_dropdown_item);
					seleccionTipo="BAÑOS";
				}else if(tipoLugar.equals("ÁREA")){
					adaptador= ArrayAdapter.createFromResource(getActivity(), R.array.array_areas, android.R.layout.simple_spinner_dropdown_item);
					seleccionTipo="ÁREA";
					
					
				}
				
				spnNombreLugar.setAdapter(adaptador);

				
			}
			
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			
			}
		});

		spnNombreLugar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				
				switch (seleccionTipo){
					
					case "EDIFICIO":
						String edificio=spnNombreLugar.getSelectedItem().toString();
						if(edificio.equals("A")){
							mapView.getMapAsync(new OnMapReadyCallback() {
								
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.024113, -92.903478);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("EDIFICIO A"));
									Toast.makeText(getActivity(), "Edificio A", Toast.LENGTH_LONG).show();
								}
							});
							
						}else if(edificio.equals("B")){
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.024088, -92.90397);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("EDIFICIO B"));
									Toast.makeText(getActivity(), "Edificio B", Toast.LENGTH_LONG).show();
								}
							});
							
						}else if(edificio.equals("C")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.024098, -92.90337);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("EDIFICIO C"));
									Toast.makeText(getActivity(), "Edificio C", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}else if(edificio.equals("E")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.02362, -92.904317);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("EDIFICIO E"));
									Toast.makeText(getActivity(), "Edificio E", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}else if(edificio.equals("L")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.02332, -92.905027);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("EDIFICIO L"));
									Toast.makeText(getActivity(), "Edificio L", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}else if(edificio.equals("M")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.023197, -92.904473);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("EDIFICIO M"));
									Toast.makeText(getActivity(), "Edificio M", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}else if(edificio.equals("H")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.023457, -92.904636);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("EDIFICIO H"));
									Toast.makeText(getActivity(), "Edificio H", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}else if(edificio.equals("I")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.023394, -92.904295);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("EDIFICIO I"));
									Toast.makeText(getActivity(), "Edificio I", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}else if(edificio.equals("K")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.023359, -92.903147);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("EDIFICIO K"));
									Toast.makeText(getActivity(), "Edificio K", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}else if(edificio.equals("N")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.023145, -92.903912);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("EDIFICIO N"));
									Toast.makeText(getActivity(), "Edificio N", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}else if(edificio.equals("Z")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.0228, -92.903601);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("EDIFICIO Z"));
									Toast.makeText(getActivity(), "Edificio Z", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}else if(edificio.equals("Ñ")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.023027, -92.903479);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("EDIFICIO Ñ"));
									Toast.makeText(getActivity(), "Edificio Ñ", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}
						
						
						break;
					
					case "DEPARTAMENTO":
						String departamento=spnNombreLugar.getSelectedItem().toString();
						
						if(departamento.equals("CENTRO DE CÓMPUTO")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.023891, -92.90286);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("CENTRO DE CÓMPUTO"));
									Toast.makeText(getActivity(), "CENTRO DE CÓMPUTO", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}else if(departamento.equals("COMUNICACIÓN Y DIFUSIÓN")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.024877, -92.904338);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("COMUNICACIÓN Y DIFUSIÓN"));
									Toast.makeText(getActivity(), "COMUNICACIÓN Y DIFUSIÓN", Toast.LENGTH_LONG).show();
								}
							});
							
						}else if(departamento.equals("CONSULTORIO MÉDICO")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.024748, -92.904527);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("CONSULTORIO MÉDICO"));
									Toast.makeText(getActivity(), "CONSULTORIO MÉDICO", Toast.LENGTH_LONG).show();
								}
							});
							
						}else if(departamento.equals("CENTRO DE INFORMACIÓN")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.023438, -92.902775);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("CENTRO DE INFORMACIÓN"));
									Toast.makeText(getActivity(), "CENTRO DE INFORMACIÓN", Toast.LENGTH_LONG).show();
								}
							});
							
						}else if(departamento.equals("PECERA")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.023565, -92.90371);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("PECERA"));
									Toast.makeText(getActivity(), "PECERA", Toast.LENGTH_LONG).show();
								}
							});
							
						}else if(departamento.equals("VINCULACIÓN")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.024677, -92.904607);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("VINCULACIÓN"));
									Toast.makeText(getActivity(), "VINCULACIÓN", Toast.LENGTH_LONG).show();
								}
							});
							
						}else if(departamento.equals("INCUBACIÓN")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.024528, -92.904427);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("INCUBACIÓN"));
									Toast.makeText(getActivity(), "INCUBACIÓN", Toast.LENGTH_LONG).show();
								}
							});
							
						}else if(departamento.equals("JURISDICCIÓN")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.024823, -92.904732);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("JURISDICCIÓN"));
									Toast.makeText(getActivity(), "JURISDICCIÓN", Toast.LENGTH_LONG).show();
								}
							});
							
						}else if(departamento.equals("ARTE Y CULTURA")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.023787, -92.90366);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("ARTE Y CULTURA"));
									Toast.makeText(getActivity(), "ARTE Y CULTURA", Toast.LENGTH_LONG).show();
								}
							});
							
						}else if(departamento.equals("CAPACITACIÓN")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.024608, -92.90433);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("CAPACITACIÓN"));
									Toast.makeText(getActivity(), "CAPACITACIÓN", Toast.LENGTH_LONG).show();
								}
							});
							
						}else if(departamento.equals("SERVICIO SOCIAL Y RESIDENCIA")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.024688, -92.90423);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("SERVICIO SOCIAL Y RESIDENCIA"));
									Toast.makeText(getActivity(), "SERVICIO SOCIAL Y RESIDENCIA", Toast.LENGTH_LONG).show();
								}
							});
							
						}else if(departamento.equals("CIENCIAS DE LA TIERRA")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.022766, -92.904413);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("CIENCIAS DE LA TIERRA"));
									Toast.makeText(getActivity(), "CIENCIAS DE LA TIERRA", Toast.LENGTH_LONG).show();
								}
							});
							
						}
						
						break;
					
					case "LABORATORIO":
						String laboratorio=spnNombreLugar.getSelectedItem().toString();
						
						if(laboratorio.equals("ELECTRÓNICA")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.023891, -92.90286);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("LABORATORIO DE ELECTRÓNICA"));
									Toast.makeText(getActivity(), "LABORATORIO DE ELECTRÓNICA", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}else if(laboratorio.equals("CÓMPUTO")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.023957, -92.902995);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("LABORATORIO DE CÓMPUTO"));
									Toast.makeText(getActivity(), "LABORATORIO DE CÓMPUTO", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}else if(laboratorio.equals("CLUB DE ROBÓTICA")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.023795, -92.903107);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("LABORATORIO DEL CLUB DE ROBÓTICA"));
									Toast.makeText(getActivity(), "LABORATORIO DEL CLUB DE ROBÓTICA", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}else if(laboratorio.equals("E-AULA")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.023225, -92.904995);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("E-AULA"));
									Toast.makeText(getActivity(), "E-AULA", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}else if(laboratorio.equals("ING. INDUSTRIAL")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.02239, -92.903668);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("LABORATORIO DE ING. INDUSTRIAL (EDIFICIO X)"));
									Toast.makeText(getActivity(), "LABORATORIO DE ING. INDUSTRIAL", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}else if(laboratorio.equals("CUALI Y CUANTI")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.022874, -92.90482);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("LABORATORIO DE CUALI Y CUANTI"));
									Toast.makeText(getActivity(), "LABORATORIO DE CUALI Y CUANTI", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}else if(laboratorio.equals("ING. CIVIL Y PETROLERA")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.022826, -92.905444);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("LABORATORIO DE ING. CIVIL Y PETROLERA"));
									Toast.makeText(getActivity(), "LABORATORIO DE ING. CIVIL Y PETROLERA", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}else if(laboratorio.equals("INVESTIGACIÓN")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.023051, -92.904793);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("LABORATORIO DE INVESTIGACIÓN"));
									Toast.makeText(getActivity(), "LABORATORIO DE INVESTIGACIÓN", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}else if(laboratorio.equals("MICROBIOLOGÍA")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.023022, -92.904894);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("LABORATORIO DE MICROBIOLOGÍA"));
									Toast.makeText(getActivity(), "LABORATORIO DE MICROBIOLOGÍA", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}else if(laboratorio.equals("QUÍMICA GENERAL")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.022414, -92.904525);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("LABORATORIO DE QUÍMICA GENERAL"));
									Toast.makeText(getActivity(), "LABORATORIO DE QUÍMICA GENERAL", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}else if(laboratorio.equals("QUÍMICA PESADA")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.022456, -92.904072);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("LABORATORIO DE QUÍMICA PESADA"));
									Toast.makeText(getActivity(), "LABORATORIO DE QUÍMICA PESADA", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}
						break;
					
					case "BAÑOS":
						
						String wc=spnNombreLugar.getSelectedItem().toString();
						
						if(wc.equals("BAÑOS EDIFICIO E")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.023595, -92.904388);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("BAÑOS EDIFICIO E"));
									Toast.makeText(getActivity(), "BAÑOS EDIFICIO E", Toast.LENGTH_LONG).show();
								}
							});
							
							
						} else if(wc.equals("BAÑOS EDIFICIO C")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.023825, -92.903303);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("BAÑOS EDIFICIO C"));
									Toast.makeText(getActivity(), "BAÑOS EDIFICIO C", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}else if(wc.equals("MOSTRAR TODOS")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng wcE = new LatLng(18.023595, -92.904388);
									mMap.addMarker(new MarkerOptions().position(wcE).title("BAÑOS EDIFICIO E"));
									
									LatLng wxC = new LatLng(18.023825, -92.903303);
									mMap.addMarker(new MarkerOptions().position(wxC).title("BAÑOS EDIFICIO C"));
									Toast.makeText(getActivity(), "TODOS LOS BAÑOS", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}
						break;
					
					case "ÁREA":
						String area=spnNombreLugar.getSelectedItem().toString();
						
						if(area.equals("ACTIVIDADES EXTRAESCOLARES")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.023066, -92.904079);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("ACTIVIDADES EXTRAESCOLARES"));
									Toast.makeText(getActivity(), "ACTIVIDADES EXTRAESCOLARES", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}else if(area.equals("GIMNASIO AUDITORIO")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.02182, -92.904401);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("GIMNASIO AUDITORIO"));
									Toast.makeText(getActivity(), "GIMNASIO AUDITORIO", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}else if(area.equals("CAFETERÍA")){
							
							mapView.getMapAsync(new OnMapReadyCallback() {
								@Override
								public void onMapReady(GoogleMap googleMap) {
									mMap=googleMap;
									mMap.clear();
									miUbicacion();
									LatLng lugar1 = new LatLng(18.02379, -92.904507);
									mMap.addMarker(new MarkerOptions().position(lugar1).title("CAFETERÍA"));
									Toast.makeText(getActivity(), "CAFETERÍA", Toast.LENGTH_LONG).show();
								}
							});
							
							
						}
						
						break;
					
					
				}
				
				
			}
			
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			
			}
		});
		
		
		
		return view;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mapView = (MapView) view.findViewById(R.id.mapaFrag);
		
		if (mapView != null) {
			
			mapView.onCreate(null);
			
			mapView.onResume();
		}
	}
	
	
	
	//activar los servicios del gps cuando esten apagados
	public void locationStart() {
		LocationManager mlocManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
		final boolean gpsEnabled = mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		if (!gpsEnabled) {
			Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(settingsIntent);
		}
		
	}
	
	public void setLocation(Location loc) {
		//Obtener la direccion de la calle a partir de la latitud y la longitud
		
		if (loc.getLatitude() != 0.0 && loc.getLongitude() != 0.0) {
			try {
				Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
				List<Address> list = geocoder.getFromLocation(
						loc.getLatitude(), loc.getLongitude(), 1);
				if (!list.isEmpty()) {
					Address DirCalle = list.get(0);
					direccion = (DirCalle.getAddressLine(0));
				}
				
			} catch (IOException e) {
				Toast.makeText(getActivity(), e.toString(),Toast.LENGTH_LONG).show();
				
				
			}
		}
	}
	
	//agregar el marcador en el mapa
	void AgregarMarcador(double lat, double lng) {
		LatLng coordenadas = new LatLng(lat, lng);
		CameraUpdate MiUbicacion = CameraUpdateFactory.newLatLngZoom(coordenadas, 17);
		if (marcador != null) marcador.remove();
		marcador = mMap.addMarker(new MarkerOptions()
				.position(coordenadas)
				.title("Dirección:" + direccion)
				.icon(BitmapDescriptorFactory.fromResource(R.drawable.makerme_opt)));
		mMap.animateCamera(MiUbicacion);
		
	}
	
	//actualizar la ubicacion
	private void ActualizarUbicacion(Location location) {
		if (location != null) {
			lat = location.getLatitude();
			lng = location.getLongitude();
			AgregarMarcador(lat, lng);
			
		}
	}
	
	//control del gps
	LocationListener locListener = new LocationListener() {
		
		@Override
		public void onLocationChanged(Location location) {
			
			ActualizarUbicacion(location);
			setLocation(location);
		}
		
		@Override
		public void onStatusChanged(String s, int i, Bundle bundle) {
		
		}
		
		@Override
		public void onProviderEnabled(String s) {
			mensaje1 = ("GPS Activado");
			Mensaje();
		}
		
		@Override
		public void onProviderDisabled(String s) {
			mensaje1 = ("GPS Desactivado");
			locationStart();
			Mensaje();
		}
	};
	
	private void miUbicacion() {
		if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(getActivity(),
					new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
					PETICION_PERMISO_LOCALIZACION);
			
			Toast.makeText(getActivity(), "Pulsa nuevamente en LocaTec para actualizar el mapa", Toast.LENGTH_LONG).show();
			return;
		} else {
			LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
			Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			ActualizarUbicacion(location);
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,1200,0,locListener);
		}
		
	}
	
	
	
	public void Mensaje() {
		Toast toast = Toast.makeText(getActivity(), mensaje1, Toast.LENGTH_LONG);
		//toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
		toast.show();
	}
	
	private void examinarTema() {
		//CREO UN INSTANACIA DE LA BASE DE DATOS
		SQLHelper conn = new SQLHelper(getActivity(),SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);
		
		//LA ESTABLESCO EN MODO LECTURA
		SQLiteDatabase db=conn.getReadableDatabase();
		
		//CREO UN CURSOR PARA RECORRER LOS CAMPOS
		Cursor cursor = db.rawQuery("SELECT estado from tema;", null);
		
		//CREO VARIABLES PARA GUARDAR LO QUE ME RETORNE EL CURSOR SEGÚN EL TIPO DE VALOR O DATO
		String estado="";
		
		while(cursor.moveToNext()){
			
			//GUARDO LOS RESULTADOS EN LAS VARIABLES
			estado  = cursor.getString(0);
			
			if(estado.equals("true")){
				//TEMA BLACK
				
				
				LinearLayout linearBase = view.findViewById(R.id.linearBaseMapa);
				linearBase.setBackgroundColor(Color.parseColor("#1F1E1E"));
				TextView ubi = view.findViewById(R.id.tvUbicacionDe);
				TextView nom = view.findViewById(R.id.tvNombreDe);
				ubi.setTextColor(Color.parseColor("#F4D121"));
				nom.setTextColor(Color.parseColor("#F4D121"));
				spnTipoLugar.setBackgroundColor(Color.WHITE);
				spnNombreLugar.setBackgroundColor(Color.WHITE);
				
				
				
				
			}else{
				//VALORES ORIGINALES
				
				
				
				
			}
			
			
		}
		
		
	}
	
	
	public float calcularDistancia(double latitud1, double logitud1, double latitud2, double longitud2) {
		
		double latitudDistancia = Math.toRadians(latitud1 - latitud2);
		double longitudDistancia = Math.toRadians(logitud1 - longitud2);
		
		double a = Math.sin(latitudDistancia / 2) * Math.sin(latitudDistancia / 2)
				+ Math.cos(Math.toRadians(latitud1)) * Math.cos(Math.toRadians(latitud2))
				* Math.sin(longitudDistancia / 2) * Math.sin(longitudDistancia / 2);
		
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		
		
		DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
		simbolos.setDecimalSeparator('.');
		DecimalFormat formateador = new DecimalFormat("000.00",simbolos);
		
		double metros = (RADIO_DE_LA_TIERRA_EN_KM * c)*1000;
		
		String dato= formateador.format(metros);
		
		float conversion = Float.parseFloat(dato);
		
		return conversion;
	}
	
	
}