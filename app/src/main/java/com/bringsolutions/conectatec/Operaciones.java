package com.bringsolutions.conectatec;

import android.content.Intent;
import android.net.Uri;
import android.widget.ImageView;

public class Operaciones {


    public Intent cambiarImagen(ImageView imagen, Uri url){

        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.setType("image/*");
        i.putExtra(Intent.EXTRA_LOCAL_ONLY,true);
        //startActivityForResult(Intent.createChooser(i,"Selecciona una foto"),1);

        Intent.createChooser(i, "Selecciona una foto...");

        return i;


    }



}
