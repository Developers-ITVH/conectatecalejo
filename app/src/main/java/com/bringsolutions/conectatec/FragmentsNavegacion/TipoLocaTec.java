package com.bringsolutions.conectatec.FragmentsNavegacion;


import android.app.FragmentManager;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bringsolutions.conectatec.BD.SQLHelper;
import com.bringsolutions.conectatec.R;
import com.bringsolutions.conectatec.tipos.locatec.CroquisTec;
import com.bringsolutions.conectatec.tipos.locatec.MapaConectaTec;

public class TipoLocaTec extends Fragment {

    View view;

    TabLayout tabs;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view =  inflater.inflate(R.layout.fragment_tipo_loca_tec, container, false);

         tabs = (TabLayout) view.findViewById(R.id.tabs);
        tabs.setTabMode(TabLayout.MODE_SCROLLABLE);


        tabs.addTab(tabs.newTab().setText("GeoMapa"));
        tabs.addTab(tabs.newTab().setText("Croquis"));

        examinarTema();

        FragmentManager fm = getFragmentManager();
        fm.beginTransaction().replace(R.id.contenedorTipoUbicacion, new MapaConectaTec()).addToBackStack(null).commit();


        tabs.setOnTabSelectedListener(
                new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {

                        int selecionPestana = tab.getPosition();
                        FragmentManager fm = getFragmentManager();

                        switch (selecionPestana){
                            case 0:
                                    fm.beginTransaction().replace(R.id.contenedorTipoUbicacion, new MapaConectaTec()).addToBackStack(null).commit();

                                    Toast.makeText(getActivity(), "GeoMapa", Toast.LENGTH_SHORT).show();

                                break;

                            case 1:

                                fm.beginTransaction().replace(R.id.contenedorTipoUbicacion, new CroquisTec()).addToBackStack(null).commit();

                                Toast.makeText(getActivity(), "CroquisTec", Toast.LENGTH_SHORT).show();

                                break;


                        }

                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        // ...
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        // ...
                    }


                }
        );

        return view;
    }

    private void examinarTema() {
        //CREO UN INSTANACIA DE LA BASE DE DATOS
        SQLHelper conn = new SQLHelper(getActivity(),SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);

        //LA ESTABLESCO EN MODO LECTURA
        SQLiteDatabase db=conn.getReadableDatabase();

        //CREO UN CURSOR PARA RECORRER LOS CAMPOS
        Cursor cursor = db.rawQuery("SELECT estado from tema;", null);

        //CREO VARIABLES PARA GUARDAR LO QUE ME RETORNE EL CURSOR SEGÚN EL TIPO DE VALOR O DATO
        String estado="";

        while(cursor.moveToNext()){

            //GUARDO LOS RESULTADOS EN LAS VARIABLES
            estado  = cursor.getString(0);

            if(estado.equals("true")){
                //TEMA BLACK

                tabs.setBackgroundColor(Color.parseColor("#35373A"));
                int[][] states = new int[][] {
                        new int[] { android.R.attr.state_enabled}, // enabled
                        new int[] {-android.R.attr.state_selected}, // disabled
                        new int[] {-android.R.attr.state_checked}, // unchecked
                        new int[] { android.R.attr.state_pressed}  // pressed
                };

                int[] colors = new int[] {
                        Color.parseColor("#FFFFFF"),
                        Color.RED,
                        Color.GREEN,
                        Color.BLUE
                };

                ColorStateList myList = new ColorStateList(states, colors);
                tabs.setTabTextColors(myList);
            }else{
                //VALORES ORIGINALES


            }


        }


    }

}