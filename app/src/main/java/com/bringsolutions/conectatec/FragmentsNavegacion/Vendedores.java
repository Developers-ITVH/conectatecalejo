package com.bringsolutions.conectatec.FragmentsNavegacion;


import android.app.Fragment;
import android.app.FragmentManager;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
//import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bringsolutions.conectatec.BD.SQLHelper;
import com.bringsolutions.conectatec.R;
import com.bringsolutions.conectatec.fragments.tipos.vendedores.Articulos;
import com.bringsolutions.conectatec.fragments.tipos.vendedores.ComidaBebida;
import com.bringsolutions.conectatec.fragments.tipos.vendedores.Multimedia;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;

import de.hdodenhof.circleimageview.CircleImageView;


public class Vendedores extends Fragment {

View view;

FrameLayout contenedor;
LinearLayout linearBase;
TabLayout tabs;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.fragment_vendedores, container, false);

        tabs = (TabLayout) view.findViewById(R.id.tabsVendedores);
        tabs.setTabMode(TabLayout.MODE_SCROLLABLE);
        contenedor = view.findViewById(R.id.contenedorVendedor);
        linearBase = view.findViewById(R.id.linearBase);
        tabs.addTab(tabs.newTab().setText("Comida"));
        tabs.addTab(tabs.newTab().setText("Servicio Multimedia"));
        tabs.addTab(tabs.newTab().setText("Artículos"));

        FragmentManager fm = getFragmentManager();
        fm.beginTransaction().replace(R.id.contenedorVendedor, new ComidaBebida()).addToBackStack(null).commit();

        examinarTema();
        tabs.setOnTabSelectedListener(
                new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {

                        int selecionPestana = tab.getPosition();
                        FragmentManager fm = getFragmentManager();

                        switch (selecionPestana){
                            case 0:

                                fm.beginTransaction().replace(R.id.contenedorVendedor, new ComidaBebida()).addToBackStack(null).commit();
/*
                             getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.contenedorVendedor, new ComidaBebida()).commit();
*/
                                break;

                            case 1:

                                fm.beginTransaction().replace(R.id.contenedorVendedor, new Multimedia()).addToBackStack(null).commit();
                               //getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.contenedorVendedor, new Multimedia()).commit();

                                break;


                            case 2:

                                fm.beginTransaction().replace(R.id.contenedorVendedor, new Articulos()).addToBackStack(null).commit();
//                                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.contenedorVendedor, new Articulos()).commit();

                                break;






                        }

                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        // ...
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        // ...
                    }


                }
        );


    lanzarTabInicial();


        return view;
    }

    void lanzarTabInicial(){

        //CREO UN INSTANACIA DE LA BASE DE DATOS
        SQLHelper conn = new SQLHelper(getActivity(),SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);

        //LA ESTABLESCO EN MODO LECTURA
        SQLiteDatabase db=conn.getReadableDatabase();

        //CREO UN CURSOR PARA RECORRER LOS CAMPOS
        Cursor cursor = db.rawQuery("SELECT estado from uso;", null);

        //CREO VARIABLES PARA GUARDAR LO QUE ME RETORNE EL CURSOR SEGÚN EL TIPO DE VALOR O DATO
        String estado="";

        while(cursor.moveToNext()){
            final CircleImageView gatito = view.findViewById(R.id.fotitoCatVendedor);
            gatito.setVisibility(View.VISIBLE);

            //GUARDO LOS RESULTADOS EN LAS VARIABLES
            estado  = cursor.getString(0);

            if(estado.equals("true")){

                TapTargetView.showFor(getActivity(), TapTarget.forView(view.findViewById(R.id.fotitoCatVendedor), "¡VENDEDORES DEL TEC!", "Pulsa en sus fotos para incrementar su tamaño, comunícate con alguno de ellos de manera rápida.")
                        .outerCircleAlpha(0.96f)
                        .titleTextSize(15)
                        .targetCircleColor(R.color.rojocirculo)
                        .titleTextColor(R.color.textoblanco)
                        .descriptionTextSize(10)  .descriptionTextAlpha(0.85f)
                        .descriptionTextColor(R.color.textoblanco)
                        .textColor(R.color.textoblanco)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.textoblanco)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(false)
                        .transparentTarget(true)

                        .targetRadius(35)     );


                //ESPERO UN SEGUNDO Y MEDIO Y MANDO A LA ACTIVIDAD PRINCIPAL
                Handler handler = new Handler();

                handler.postDelayed(new Runnable() {
                    public void run() {

                        gatito.setVisibility(View.GONE);
                    }
                }, 1500);
            }else{
                gatito.setVisibility(View.GONE);

            }



        }


    }

    private void examinarTema() {
//CREO UN INSTANACIA DE LA BASE DE DATOS
        SQLHelper conn = new SQLHelper(getActivity(),SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);

        //LA ESTABLESCO EN MODO LECTURA
        SQLiteDatabase db=conn.getReadableDatabase();

        //CREO UN CURSOR PARA RECORRER LOS CAMPOS
        Cursor cursor = db.rawQuery("SELECT estado from tema;", null);

        //CREO VARIABLES PARA GUARDAR LO QUE ME RETORNE EL CURSOR SEGÚN EL TIPO DE VALOR O DATO
        String estado="";

        while(cursor.moveToNext()){

            //GUARDO LOS RESULTADOS EN LAS VARIABLES
            estado  = cursor.getString(0);

            if(estado.equals("true")){
                //TEMA BLACK
                contenedor.setBackgroundColor(Color.parseColor("#232323"));
                linearBase.setBackgroundColor(Color.parseColor("#232323"));
                tabs.setBackgroundColor(Color.parseColor("#35373A"));
                int[][] states = new int[][] {
                        new int[] { android.R.attr.state_enabled}, // enabled
                        new int[] {-android.R.attr.state_selected}, // disabled
                        new int[] {-android.R.attr.state_checked}, // unchecked
                        new int[] { android.R.attr.state_pressed}  // pressed
                };

                int[] colors = new int[] {
                        Color.parseColor("#FFFFFF"),
                        Color.RED,
                        Color.GREEN,
                        Color.BLUE
                };

                ColorStateList myList = new ColorStateList(states, colors);
                tabs.setTabTextColors(myList);

            }else{
                //VALORES ORIGINALES

                contenedor.setBackgroundColor(Color.parseColor("#FFFFFF"));
                linearBase.setBackgroundColor(Color.parseColor("#FFFFFF"));
                tabs.setBackgroundColor(Color.parseColor("#9dde7e"));
                tabs.setTabTextColors(R.color.colorPrimaryDark, R.color.textotitlecirculo);


            }


        }


    }

}
