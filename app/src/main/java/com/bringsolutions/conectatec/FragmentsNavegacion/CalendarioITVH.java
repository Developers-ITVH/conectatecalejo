package com.bringsolutions.conectatec.FragmentsNavegacion;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.bringsolutions.conectatec.R;


public class CalendarioITVH extends Fragment {
	
	View view;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		view = inflater.inflate(R.layout.fragment_calendario_itvh, container, false);
		
		
		WebView myWebView = (WebView) view.findViewById(R.id.webViewCalendario);
		WebSettings webSettings = myWebView.getSettings();
		webSettings.setJavaScriptEnabled(true);
		myWebView.setWebViewClient(new WebViewClient());
		myWebView.setWebChromeClient(new WebChromeClient());
		myWebView.loadUrl("http://cc.itvillahermosa.edu.mx/sys/estpro/calendario/");
		
		
		return view;
		}
	
}
