package com.bringsolutions.conectatec.FragmentsNavegacion;



import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
//import android.support.v4.app.Fragment;
import android.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bringsolutions.conectatec.BD.SQLHelper;
import com.bringsolutions.conectatec.MainActivity;
import com.bringsolutions.conectatec.R;
import com.bringsolutions.conectatec.clases.chat.AdapterMensajes;
import com.bringsolutions.conectatec.clases.chat.MensajeEnviar;
import com.bringsolutions.conectatec.clases.chat.MensajeRecibir;
import com.bringsolutions.conectatec.objetos.CONSTANTES;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;
import static com.bringsolutions.conectatec.objetos.CONSTANTES.USUARIO;

public class ChaTec extends Fragment {
    String  estado="";
    View view;
    private CircleImageView fotoPerfil;
    private TextView nombre;
    private RecyclerView rvMensajes;
    private EditText txtMensaje;
    private Button btnEnviar;
    private AdapterMensajes adapter;
    private ImageButton btnEnviarFoto;
    private FirebaseDatabase database;
    private DatabaseReference databaseReference;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private static final int PHOTO_SEND = 1;
    private ProgressDialog progressDialog;

    CardView bannerChat;
    LinearLayout linearCuerpoChat;
    LinearLayout respaldoBanner;

    JsonObjectRequest jsonObjectRequest;
    RequestQueue requestQueue;
    
    
    ImageButton ibGrupo, ibPagina, ibWhats;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_cha_tec, container, false);
        
        ibGrupo =view.findViewById(R.id.fbGrupo);
        ibPagina =view.findViewById(R.id.fbPagina);
        ibWhats =view.findViewById(R.id.fbWhatsApp);
        clickRedes();
    
    
        final TextView reglas = view.findViewById(R.id.textoReglas);
        final  String[] reglitas = new String[8];

        reglitas[0] = "   [Mal comportamiento = bloqueo definitivo]";
        reglitas[1] = "   [Spam = bloqueo definitivo]";
        reglitas[2] = "   [Sé respetuoso]";
        reglitas[3] = "   [No compartas datos personales por aquí]";
        reglitas[4] = "   [El chat se vacía periódicamente]";
        reglitas[5] = "   +BringSolutions+";
        reglitas[6] = "   [Únete a las redes sociales del ITVH]";
        reglitas[7] = "   [Si tienes algún problema, comunícante con nosotros]";

        final Handler handler = new Handler();

        Timer timer = new Timer();

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            int numFrase = (int) (Math.random() * reglitas.length);

                            reglas.setText(reglitas[numFrase]);

                            //verificarBloqueo(USUARIO.getCorreo());


                        }catch (Exception e){

                        }
                    }
                });
            }
        };
        timer.schedule(task,0,2000);


        LinearLayout  linearLayoutBannerFondo=(LinearLayout) view.findViewById(R.id.linearBannerUsuariosChat);
        bannerChat = view.findViewById(R.id.cardviewBanner);
        linearCuerpoChat = view.findViewById(R.id.cuerpoMensajes);
        respaldoBanner= view.findViewById(R.id.respaldoBanner);

        progressDialog=new ProgressDialog(getActivity());
        fotoPerfil = (CircleImageView) view.findViewById(R.id.fotoPerfil);

        Glide.with(getActivity().getApplicationContext()).load(Uri.parse(USUARIO.getFoto())).into(fotoPerfil);
        nombre = (TextView) view.findViewById(R.id.nombre);
        rvMensajes = (RecyclerView) view.findViewById(R.id.rvMensajes);
        txtMensaje = (EditText) view.findViewById(R.id.txtMensaje);
        btnEnviar = (Button) view.findViewById(R.id.btnEnviar);
        btnEnviarFoto = (ImageButton) view.findViewById(R.id.btnEnviarFoto);

        nombre.setText(USUARIO.getNombre());
        database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference("chat");//Sala de chat (nombre)
        storage = FirebaseStorage.getInstance();

        //Volley
        requestQueue = Volley.newRequestQueue(getActivity());

        examinarTema();

        expacionFotoPerfilChat();




        if(Integer.parseInt(USUARIO.getTipo())==2){

            btnEnviarFoto.setVisibility(View.VISIBLE);



        }else if(Integer.parseInt(USUARIO.getTipo())==1){
            btnEnviarFoto.setVisibility(View.GONE);


        }
        adapter = new AdapterMensajes(getActivity());
        LinearLayoutManager l = new LinearLayoutManager(getActivity());
        rvMensajes.setLayoutManager(l);
        rvMensajes.setAdapter(adapter);

        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (txtMensaje.getText().toString().isEmpty()){

                    Toast.makeText(getActivity().getApplicationContext(), "Introduce un texto jaguar", Toast.LENGTH_SHORT).show();
                }else {
                    databaseReference.push().setValue(new MensajeEnviar(txtMensaje.getText().toString(), nombre.getText().toString(), USUARIO.getFoto(), "1",USUARIO.getCorreo(), ServerValue.TIMESTAMP));
                    txtMensaje.setText("");
                }



            }
        });

        btnEnviarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.setType("image/*");
                i.putExtra(Intent.EXTRA_LOCAL_ONLY,true);
                startActivityForResult(Intent.createChooser(i,"Selecciona una foto"),PHOTO_SEND);
            }
        });


        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                setScrollbar();
            }
        });


        databaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                MensajeRecibir m = dataSnapshot.getValue(MensajeRecibir.class);

                adapter.addMensaje(m);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        return view;
    }
    
    private void clickRedes() {
        
        ibGrupo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String facebookId = "fb://group/itvhJET";
                String urlPage = "https://www.facebook.com/groups/itvhJET/";
    
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(facebookId)));
                } catch (Exception e) {
                    Toast.makeText(getActivity().getApplicationContext(), "Aplicación no instalada",  Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(urlPage)));
                }
        
            }
        });
        
        ibPagina.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
    
                String facebookId = "fb://page/104040507607452";
                String urlPage = "https://www.facebook.com/Instituto-Tecnológico-De-México-En-Villahermosa-TECNM-104040507607452/";
    
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(facebookId)));
                } catch (Exception e) {
                    Toast.makeText(getActivity().getApplicationContext(), "Aplicación no instalada",  Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(urlPage)));
                }
            }
        });
        
        ibWhats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://chat.whatsapp.com/1dZjnvZ7cm8DwtvRLIvVjc");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
    }
    
    private void verificarBloqueo(String correo) {


        final String url =    CONSTANTES.URL_PRINCIPAL+ "verificar_bloqueo.php?correo="+correo;

        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                JSONArray json = response.optJSONArray("bloqueado");
                JSONObject jsonObject = null;

                try {
                    jsonObject=json.getJSONObject(0);

                      estado =jsonObject.optString("estado");


                    if(estado.equals("2")){
                        txtMensaje.setEnabled(false);
                        txtMensaje.setHint(" ESTÁS BLOQUEADO");
                        txtMensaje.setHintTextColor(Color.RED);
                        btnEnviar.setEnabled(false);
                        btnEnviar.setTextColor(Color.RED);
                        btnEnviarFoto.setEnabled(false);

                    }else  if(estado.equals("1")){
                        txtMensaje.setEnabled(true);
                        txtMensaje.setHint(" Escribe un mensaje...");
                        btnEnviar.setText("Enviar");
                        txtMensaje.setHintTextColor(Color.parseColor("#b1b1b1"));
                        btnEnviar.setTextColor(Color.WHITE);
                        btnEnviar.setEnabled(true);
                        btnEnviarFoto.setEnabled(true);
                    }


                } catch (JSONException e) {
                    Toast.makeText(getActivity(),"No se pudo: " + e.toString(),Toast.LENGTH_LONG).show();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if(estado.equals("1")){
                    txtMensaje.setEnabled(true);
                    btnEnviar.setEnabled(true);
                    btnEnviarFoto.setEnabled(true);

                }


            }
        });

        requestQueue.add(jsonObjectRequest);
    }

    private void examinarTema() {
//CREO UN INSTANACIA DE LA BASE DE DATOS
        SQLHelper conn = new SQLHelper(getActivity(),SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);

        //LA ESTABLESCO EN MODO LECTURA
        SQLiteDatabase db=conn.getReadableDatabase();

        //CREO UN CURSOR PARA RECORRER LOS CAMPOS
        Cursor cursor = db.rawQuery("SELECT estado from tema;", null);

        //CREO VARIABLES PARA GUARDAR LO QUE ME RETORNE EL CURSOR SEGÚN EL TIPO DE VALOR O DATO
        String estado="";

        while(cursor.moveToNext()){

            //GUARDO LOS RESULTADOS EN LAS VARIABLES
            estado  = cursor.getString(0);

            if(estado.equals("true")){
                //TEMA BLACK

                bannerChat.setCardBackgroundColor(Color.parseColor("#35373A"));
                nombre.setTextColor(Color.parseColor("#0ED300"));
                linearCuerpoChat.setBackgroundColor(Color.parseColor("#232323"));

                int[][] states = new int[][] {
                        new int[] { android.R.attr.state_enabled}, // enabled
                        new int[] {-android.R.attr.state_selected}, // disabled
                        new int[] {-android.R.attr.state_checked}, // unchecked
                        new int[] { android.R.attr.state_pressed}  // pressed
                };

                int[] colors = new int[] {
                        Color.parseColor("#FFFFFF"),
                        Color.RED,
                        Color.GREEN,
                        Color.BLUE
                };

                ColorStateList myList = new ColorStateList(states, colors);
                btnEnviarFoto.setBackgroundTintList(myList);
                txtMensaje.setHintTextColor(Color.parseColor("#939393"));
                txtMensaje.setTextColor(Color.WHITE);
                respaldoBanner.setBackgroundColor(Color.parseColor("#191919"));

            }else{
                //VALORES ORIGINALES

                bannerChat.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
                nombre.setTextColor(Color.parseColor("#000000"));
                linearCuerpoChat.setBackgroundColor(Color.parseColor("#FFFFFF"));
                int[][] states = new int[][] {
                        new int[] { android.R.attr.state_enabled}, // enabled
                        new int[] {-android.R.attr.state_selected}, // disabled
                        new int[] {-android.R.attr.state_checked}, // unchecked
                        new int[] { android.R.attr.state_pressed}  // pressed
                };

                int[] colors = new int[] {
                        Color.parseColor("#03BD65"),
                        Color.RED,
                        Color.GREEN,
                        Color.BLUE
                };

                ColorStateList myList = new ColorStateList(states, colors);
                btnEnviarFoto.setBackgroundTintList(myList);


            }
        }


    }

    private void expacionFotoPerfilChat() {

        fotoPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Service.LAYOUT_INFLATER_SERVICE);

                final Dialog builder = new Dialog(getActivity());
                builder.requestWindowFeature(Window.FEATURE_NO_TITLE);

                View dialoglayout = inflater.inflate(R.layout.imagen_dialog, null);
                ImageView imagenInflada = dialoglayout.findViewById(R.id.imagenDialog);

                builder.setContentView(dialoglayout);

                Glide.with(getActivity()).load(Uri.parse(USUARIO.getFoto())).into(imagenInflada);

                builder.show();
            }
        });
    }

    private void setScrollbar(){
        rvMensajes.scrollToPosition(adapter.getItemCount()-1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Uri u = data.getData();
try{

    storageReference = storage.getReference("imagenes_chat");//FOTOS SEGÚN SEA EL CASO
    progressDialog.setTitle("Subiendo tu foto");
    progressDialog.setMessage("Subiendo foto, aguanta...");
    progressDialog.setCancelable(false);
    progressDialog.show();

    final StorageReference fotoReferencia = storageReference.child(u.getLastPathSegment());

    UploadTask uploadTask = fotoReferencia.putFile(u);



    Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
        @Override
        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
            if (!task.isSuccessful()) {
                throw task.getException();
            }

            return fotoReferencia.getDownloadUrl();
        }
    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
        @Override
        public void onComplete(@NonNull Task<Uri> task) {

            progressDialog.hide();

            Toast.makeText(getActivity(),"Listo Jarguar!", Toast.LENGTH_SHORT).show();

            Uri pSend = task.getResult();

            MensajeEnviar m = new MensajeEnviar("     "+USUARIO.getNombre()+" ha enviado una foto",pSend.toString(),nombre.getText().toString(),USUARIO.getFoto(),"2",USUARIO.getCorreo(),ServerValue.TIMESTAMP);

            databaseReference.push().setValue(m);

        }
    });



    }catch(Exception e) {
    progressDialog.hide();

            Toast.makeText(getActivity(), "Ocurrio un problema a la hora de subir tu foto.", Toast.LENGTH_SHORT).show();

        }

        }









}
