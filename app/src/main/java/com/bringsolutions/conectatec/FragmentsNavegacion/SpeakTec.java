package com.bringsolutions.conectatec.FragmentsNavegacion;


import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.content.Intent;
import android.os.Build;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.app.Fragment;
import android.widget.Toast;
import com.bringsolutions.conectatec.BD.SQLHelper;
import com.bringsolutions.conectatec.R;
import com.bringsolutions.conectatec.objetos.Respuestas;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import java.text.Normalizer;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;
import static com.bringsolutions.conectatec.objetos.CONSTANTES.USUARIO;


public class SpeakTec extends Fragment implements TextToSpeech.OnInitListener {

    View view ;
    private static final int RECONOCEDOR_VOZ = 7;
    private TextView escuchando;
    private TextView respuesta;
    private ArrayList<Respuestas> respuest;
    private TextToSpeech leer;
    private ImageView microfono;
    RelativeLayout contenedor;
CardView tarjetaEscuchado, tarjetaRespuesta;
TextView tEscuchado, tRespuesta;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.fragment_speak_tec, container, false);
        contenedor = view.findViewById(R.id.contenedorSpeak);
        tarjetaEscuchado= view.findViewById(R.id.cardEscuchado);
        tarjetaRespuesta = view.findViewById(R.id.cardRespuesta);
        tEscuchado  = view.findViewById(R.id.titleEscuchado);
        tRespuesta = view. findViewById(R.id.titleRespuesta);
        inicializar();

        examinarTema();


        microfono.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hablar(v);
            }
        });


lanzarTabInicial();

        return view;
    }

    void verificarPermisoRecoder(){

        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.RECORD_AUDIO)!= PackageManager.PERMISSION_GRANTED) {


                ActivityCompat.requestPermissions(getActivity(),new String[]{android.Manifest.permission.RECORD_AUDIO}, RECONOCEDOR_VOZ);

        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && requestCode == RECONOCEDOR_VOZ){
            ArrayList<String> reconocido = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String escuchado = reconocido.get(0);
            escuchando.setText(escuchado);
            prepararRespuesta(escuchado);
        }
    }

    void prepararRespuesta(String escuchado) {
        String normalizar = Normalizer.normalize(escuchado, Normalizer.Form.NFD);
        String sintilde = normalizar.replaceAll("[^\\p{ASCII}]", "");

        int resultado;
        String respuesta = respuest.get(0).getRespuestas();
        for (int i = 0; i < respuest.size(); i++) {
            resultado = sintilde.toLowerCase().indexOf(respuest.get(i).getCuestion());
            if(resultado != -1){
                respuesta = respuest.get(i).getRespuestas();
            }
        }
        responder(respuesta);
    }

    void responder(String respuestita) {
        respuesta.setText(respuestita);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            leer.speak(respuestita, TextToSpeech.QUEUE_FLUSH, null, null);
        }else {
            leer.speak(respuestita, TextToSpeech.QUEUE_FLUSH, null);
        }
    }

    public void inicializar(){
        microfono  = (ImageView) view.findViewById(R.id.botonMicro);

        escuchando = (TextView)view.findViewById(R.id.tvEscuchado);
        respuesta = (TextView)view.findViewById(R.id.tvRespuesta);
        respuest = proveerDatos();
        Context c = getActivity().getApplication();
         leer = new TextToSpeech(c, this);
         verificarPermisoRecoder();


/*
            TapTargetView.showFor(getActivity(), TapTarget.forView(view.findViewById(R.id.botonMicro), "¡PREGÚNTA!", "Di palabras que consideres claves o preguntas enfocadas a términos específicos.")   .outerCircleColor(R.color.versiontres)      // Specify a color for the outer circle
                    .outerCircleAlpha(0.96f)            // Specify the alpha amount for the outer circle
                    .targetCircleColor(R.color.colorsiete)   // Specify a color for the target circle
                    .titleTextSize(40)                  // Specify the size (in sp) of the title text
                    .titleTextColor(R.color.colorsiete)      // Specify the color of the title text
                    .descriptionTextSize(30)            // Specify the size (in sp) of the description text
                    .descriptionTextColor(R.color.colorsiete)  // Specify the color of the description text
                    .textColor(R.color.colorsiete)            // Specify a color for both the title and description text
                    .textTypeface(Typeface.SANS_SERIF)  // Specify a typeface for the text
                    .dimColor(R.color.coloruno)            // If set, will dim behind the view with 30% opacity of the given color
                    .drawShadow(true)                   // Whether to draw a drop shadow or not
                    .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
                    .tintTarget(true)                   // Whether to tint the target view's color
                    .transparentTarget(false)           // Specify whether the target is transparent (displays the content underneath)
                                 // Specify a custom drawable to draw as the target
                    .targetRadius(60)     );
*/

    }

    public void hablar(View v){
        Intent hablar = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        hablar.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "es-MX");
        startActivityForResult(hablar, RECONOCEDOR_VOZ);
    }

    public ArrayList<Respuestas> proveerDatos(){
        ArrayList<Respuestas> respuestas = new ArrayList<>();
        respuestas.add(new Respuestas("defecto", "¡Aún no estoy programada para responder eso. Lo siento!"));
        respuestas.add(new Respuestas("hola", "¡Hola "+USUARIO.getNombre()+"! ¡Qué tal!"));
        respuestas.add(new Respuestas("funcion", "Brindar información de apoyo para los estudiantes del Instituto Tecnológico de Villahermosa."));
        respuestas.add(new Respuestas("ingenieria en sistemas", "¡La mejor carrera!"));
        respuestas.add(new Respuestas("padre", "Mi padre es Alejo"));
        respuestas.add(new Respuestas("gracias", "Espero haber sido de ayuda jejeje."));
        respuestas.add(new Respuestas("nos vemos", "¡Hasta luego, "+USUARIO.getNombre()+"! Espero haber sido de ayuda."));
        respuestas.add(new Respuestas("como estas", "Emocionada por ser de ayuda para ti, vamos, pregúntame algo."));
        respuestas.add(new Respuestas("nombre", "Alejo me puso: Alejita"));
        respuestas.add(new Respuestas("complementarias", "Son cinco actividades que todo estudiante debe realizar en los primeros seis semestres como máximo, de no realizarlas, no podrá reinscribirse a séptimo semestre y la alternativa será  una prórroga válida para un semestre más con carga mínima."+"\n\nPROCEDIMIENTO DE TRÁMITE:\n\n"+"1. Descargar la Solicitud de la complementaria en el Sistema de Actividades Complementarias (SAC)\n" +
                "2. Entregar dos juegos en División de Estudios, uno se queda ahí y el otro, a ti. \n" +
                "3. Una vez terminada la Actividad descarga 4 juegos de la Constancia de Acreditación \n" +
                "4. Entregar un juego con el maestro a cargo de la actividad, un juego en División de Estudios, y guardar los dos restantes\n" +
                "5. Los dos juegos deberán tener dos sellos y verificar que el crédito esté en el sistema."));
        respuestas.add(new Respuestas("como te llamas", "Alejo me puso: Alejita"));
        respuestas.add(new Respuestas("ingles", "Son diez niveles que todo estudiante debe cursar como requisito para la titulación."+"\n\n"+"El V P A, es un examen para liberar el inglés que se presenta dos veces por semestre \n" +
                "De acuerdo al desempeño evaluado en puntos, se promediará el inglés para saber si eres candidato apto."));
        respuestas.add(new Respuestas("quien eres", "Me llamo Alejita."));
        respuestas.add(new Respuestas("conectatec", "Es una aplicación dedicada a los estudiantes con el fin de brindar información de los vendedores. Además de contar con herramientas como un chat, síe, complementarias, inglés, avisos y LocaTec."));
        respuestas.add(new Respuestas("extraescolares", "Deportivas:\n" +
                "Volibol\n" +
                "Fútbol \n" +
                "Básquetbol \n" +
                "Béisbol \n" +
                "Acondicionamiento físico\n" +
                "Ajedrez.\n\n" +
                "Culturales:\n" +
                "Rondalla \n" +
                "Baile moderno \n" +
                "Folclor"));

        return respuestas;
    }

    void lanzarTabInicial(){

        if(verificarConteoBD().equals("1")){

            //CREO UN INSTANACIA DE LA BASE DE DATOS
            SQLHelper conn = new SQLHelper(getActivity(),SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);

            //LA ESTABLESCO EN MODO LECTURA
            SQLiteDatabase db=conn.getReadableDatabase();

            //CREO UN CURSOR PARA RECORRER LOS CAMPOS
            Cursor cursor = db.rawQuery("SELECT estado from uso;", null);

            //CREO VARIABLES PARA GUARDAR LO QUE ME RETORNE EL CURSOR SEGÚN EL TIPO DE VALOR O DATO
            String estado="";

            while(cursor.moveToNext()){

                //GUARDO LOS RESULTADOS EN LAS VARIABLES
                estado  = cursor.getString(0);

                if(estado.equals("true")){

                    TapTargetView.showFor(getActivity(), TapTarget.forView(view.findViewById(R.id.botonMicro), "¡RESUELVE TUS DUDAS!", "Usa términos específicos y fáciles. SpeakTec te brinda información respecto a preguntas o dudas más frecuentes: Complementarias, Servicio, Inglés, SAM, entre otras cosas...   ")
                            .outerCircleAlpha(0.96f)
                            .titleTextSize(20)
                            .targetCircleColor(R.color.rojocirculo)
                            .titleTextColor(R.color.textoblanco)
                            .descriptionTextSize(15)  .descriptionTextAlpha(0.85f)
                            .descriptionTextColor(R.color.textoblanco)
                            .textColor(R.color.textoblanco)
                            .textTypeface(Typeface.SANS_SERIF)
                            .dimColor(R.color.textoblanco)
                            .drawShadow(true)
                            .cancelable(false)
                            .tintTarget(false)
                            .transparentTarget(true)

                            .targetRadius(35)     );

                    db.execSQL("UPDATE contador SET cuentaspeak='2';");

                }else{

                    Toast.makeText(getActivity(), "¡Qué dudas tienes " + USUARIO.getNombre()+"!" , Toast.LENGTH_SHORT).show();

                }
            }
        }



    }

    public String verificarConteoBD(){
        String valor="";

//CREO UN INSTANACIA DE LA BASE DE DATOS
        SQLHelper conn = new SQLHelper(getActivity(),SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);

        //LA ESTABLESCO EN MODO LECTURA
        SQLiteDatabase db=conn.getReadableDatabase();

        //CREO UN CURSOR PARA RECORRER LOS CAMPOS
        Cursor cursor = db.rawQuery("SELECT cuentaspeak from contador;", null);

        //CREO VARIABLES PARA GUARDAR LO QUE ME RETORNE EL CURSOR SEGÚN EL TIPO DE VALOR O DATO
        String estado="";

        while(cursor.moveToNext()){

            //GUARDO LOS RESULTADOS EN LAS VARIABLES
            estado  = cursor.getString(0);

            if(estado.equals("1")){

                valor="1";
            }else{

                valor="2";



            }


        }

        return valor;

    }


    private void examinarTema() {
        //CREO UN INSTANACIA DE LA BASE DE DATOS
        SQLHelper conn = new SQLHelper(getActivity(),SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);

        //LA ESTABLESCO EN MODO LECTURA
        SQLiteDatabase db=conn.getReadableDatabase();

        //CREO UN CURSOR PARA RECORRER LOS CAMPOS
        Cursor cursor = db.rawQuery("SELECT estado from tema;", null);

        //CREO VARIABLES PARA GUARDAR LO QUE ME RETORNE EL CURSOR SEGÚN EL TIPO DE VALOR O DATO
        String estado="";

        while(cursor.moveToNext()){

            //GUARDO LOS RESULTADOS EN LAS VARIABLES
            estado  = cursor.getString(0);

            if(estado.equals("true")){
                //TEMA BLACK

                contenedor.setBackgroundColor(Color.parseColor("#232323"));
                tarjetaRespuesta.setCardBackgroundColor(Color.parseColor("#1F1E1E"));
                tarjetaEscuchado.setCardBackgroundColor(Color.parseColor("#1F1E1E"));
                tEscuchado.setTextColor(Color.parseColor("#FFFFFF"));
                tRespuesta.setTextColor(Color.parseColor("#FFFFFF"));

                escuchando.setTextColor(Color.parseColor("#F4D121"));
                respuesta.setTextColor(Color.parseColor("#F4D121"));

            }else{
                //VALORES ORIGINALES




            }


        }


    }
    @Override
    public void onInit(int status) {

    }
}
