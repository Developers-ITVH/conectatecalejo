package com.bringsolutions.conectatec;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class AcercaDe extends AppCompatActivity {

    ImageView pagina, face, alejo, machin;

    TextView textoAviso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acerca_de);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    pagina = findViewById(R.id.btnPaginaWeb);
    face = findViewById(R.id.btnFacebook);
    textoAviso = findViewById(R.id.textoAvisoPrivacidad);
    alejo= findViewById(R.id.btnAlejo);
    machin= findViewById(R.id.btnMachin);



    pagina.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Uri uri = Uri.parse("https://www.bringsolutions.com.mx/");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);

            startActivity(intent);
        }
    });
        alejo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://api.whatsapp.com/send?phone=+529934341808");
                startActivity( new Intent(Intent.ACTION_VIEW, uri));
                Toast.makeText(getApplicationContext(), "Se abrirá en WhatsApp", Toast.LENGTH_SHORT).show();
            }
        });
        machin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://api.whatsapp.com/send?phone=+529933951421");
                startActivity( new Intent(Intent.ACTION_VIEW, uri));
                Toast.makeText(getApplicationContext(), "Se abrirá en WhatsApp", Toast.LENGTH_SHORT).show();
            }
        });


    face.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Uri uri = Uri.parse("https://www.facebook.com/bgsolutionstec/");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        }
    });


    textoAviso.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Uri uri = Uri.parse("https://joseromanmachinblog.wordpress.com/politica-de-privacidad/");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        }
    });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }

}
