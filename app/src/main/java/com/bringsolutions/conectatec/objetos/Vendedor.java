package com.bringsolutions.conectatec.objetos;

public class Vendedor {

    private String idVendedor;
    private String horaIncio;
    private String tiempoInicio;
    private String horaFinal;
    private String tiempoFinal;
    private String tipoVendedor;
    private String descripcion;
    private String fotoUno;
    private String fotoDos;
    private String fotoTres;
    private String correo;

    private String fotoPerfilUsuario;
    private String nombreUsuario;
    private String telefonoUsuario;


    public Vendedor(){}

    public Vendedor(String horaIncio, String tiempoInicio, String horaFinal, String tiempoFinal, String descripcion, String fotoUno, String fotoDos, String fotoTres, String fotoPerfilUsuario, String nombreUsuario, String telefonoUsuario) {
        this.horaIncio = horaIncio;
        this.tiempoInicio = tiempoInicio;
        this.horaFinal = horaFinal;
        this.tiempoFinal = tiempoFinal;
        this.descripcion = descripcion;
        this.fotoUno = fotoUno;
        this.fotoDos = fotoDos;
        this.fotoTres = fotoTres;
        this.fotoPerfilUsuario = fotoPerfilUsuario;
        this.nombreUsuario = nombreUsuario;
        this.telefonoUsuario = telefonoUsuario;
    }

    public String getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(String idVendedor) {
        this.idVendedor = idVendedor;
    }

    public String getHoraIncio() {
        return horaIncio;
    }

    public void setHoraIncio(String horaIncio) {
        this.horaIncio = horaIncio;
    }

    public String getTiempoInicio() {
        return tiempoInicio;
    }

    public void setTiempoInicio(String tiempoInicio) {
        this.tiempoInicio = tiempoInicio;
    }

    public String getHoraFinal() {
        return horaFinal;
    }

    public void setHoraFinal(String horaFinal) {
        this.horaFinal = horaFinal;
    }

    public String getTiempoFinal() {
        return tiempoFinal;
    }

    public void setTiempoFinal(String tiempoFinal) {
        this.tiempoFinal = tiempoFinal;
    }

    public String getTipoVendedor() {
        return tipoVendedor;
    }

    public void setTipoVendedor(String tipoVendedor) {
        this.tipoVendedor = tipoVendedor;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFotoUno() {
        return fotoUno;
    }

    public void setFotoUno(String fotoUno) {
        this.fotoUno = fotoUno;
    }

    public String getFotoDos() {
        return fotoDos;
    }

    public void setFotoDos(String fotoDos) {
        this.fotoDos = fotoDos;
    }

    public String getFotoTres() {
        return fotoTres;
    }

    public void setFotoTres(String fotoTres) {
        this.fotoTres = fotoTres;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getFotoPerfilUsuario() {
        return fotoPerfilUsuario;
    }

    public void setFotoPerfilUsuario(String fotoPerfilUsuario) {
        this.fotoPerfilUsuario = fotoPerfilUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getTelefonoUsuario() {
        return telefonoUsuario;
    }

    public void setTelefonoUsuario(String telefonoUsuario) {
        this.telefonoUsuario = telefonoUsuario;
    }
}
