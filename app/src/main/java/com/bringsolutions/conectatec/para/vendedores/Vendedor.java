package com.bringsolutions.conectatec.para.vendedores;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import static com.bringsolutions.conectatec.objetos.CONSTANTES.USUARIO;
import static com.bringsolutions.conectatec.objetos.CONSTANTES.VENDEDOR;
import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bringsolutions.conectatec.BD.SQLHelper;
import com.bringsolutions.conectatec.R;
import com.bringsolutions.conectatec.objetos.CONSTANTES;
import com.bumptech.glide.Glide;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;

import de.hdodenhof.circleimageview.CircleImageView;

public class Vendedor extends AppCompatActivity {

    private Animation smalltobig, btta, btta2, atg, packageimg,bganim;


    final int PHOTO_ONE=1, PHOTO_TWO=2, PHOTO_TREE=3;
    String foto1, foto2, foto3;
    ProgressDialog progressDialog;

    String uriFinal;

    CircleImageView imagenVendedor;

    ImageView productoUno, productoDos, productoTres;

    FloatingActionButton botonEditarInformacion;

    TextView vistaHorario, vistaNombre, vistaTipoVendedor, vistaDescripcion;

        //WEB SERVICE
    RequestQueue requestQueue;
    StringRequest stringRequest;
    JsonObjectRequest jsonObjectRequest;

    //fotos
    private FirebaseStorage storage;
    private StorageReference storageReference;

    //DIALOG INFORMACION VENDEDOR
    Spinner horaInicioSpinner, tiempoInicioSpinner, horaFinalSpinner, tiempoFinalSpinner, tipoVendedorSpinner;
    Button btnGuardarInformacionVendedor;
    EditText cajaDescripcion;
    ImageButton cerrarDialogo;

    //LOTTIE
    LottieAnimationView animacionLottie;
    LinearLayout linearLottieAnim;

    RelativeLayout linearBase;

    CardView informacionTarjeta;

    TextView tituloN, tituloH, tituloTipoV, tituloDescrip, tituloFotitos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendedor);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    
        inicializar();
        clicksBotonesEnGeneral();
        buscarVendedor(USUARIO.getCorreo());


        //CREO UN INSTANACIA DE LA BASE DE DATOS
        SQLHelper conn = new SQLHelper(getApplicationContext(),SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);

        //LA ESTABLESCO EN MODO LECTURA
        SQLiteDatabase db=conn.getReadableDatabase();

        //CREO UN CURSOR PARA RECORRER LOS CAMPOS
        Cursor cursor = db.rawQuery("SELECT estado from uso;", null);

        //CREO VARIABLES PARA GUARDAR LO QUE ME RETORNE EL CURSOR SEGÚN EL TIPO DE VALOR O DATO
        String estado="";

        while(cursor.moveToNext()){

            //GUARDO LOS RESULTADOS EN LAS VARIABLES
            estado  = cursor.getString(0);

            if(estado.equals("true") && verificarConteoBD().equals("1")){

                TapTargetView.showFor(Vendedor.this, TapTarget.forView(findViewById(R.id.linearsTresFotos), "¡MUESTRA TUS PRODUCTOS!", "Sube primero las fotos de tus productos, luego especifica tu información.   ")
                        .outerCircleAlpha(0.96f)
                        .titleTextSize(20)
                        .targetCircleColor(R.color.rojocirculo)
                        .titleTextColor(R.color.textoblanco)
                        .descriptionTextSize(15)  .descriptionTextAlpha(0.85f)
                        .descriptionTextColor(R.color.textoblanco)
                        .textColor(R.color.textoblanco)
                        .textTypeface(Typeface.SANS_SERIF)
                        .dimColor(R.color.textoblanco)
                        .drawShadow(true)
                        .cancelable(false)
                        .tintTarget(false)
                        .transparentTarget(true)

                        .targetRadius(90)     );

                db.execSQL("UPDATE contador SET cuenta='2';");


            }else{
                Toast.makeText(getApplicationContext(), "¡Qué hay de nuevo " + USUARIO.getNombre()+"!" , Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }

    void clicksBotonesEnGeneral(){

        botonEditarInformacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(foto1==null || foto2==null || foto3==null){
                    Toast.makeText(getApplicationContext(),"Sube tus fotos primeramente =)", Toast.LENGTH_LONG).show();

                }else{

                    LayoutInflater inflater = getLayoutInflater();

                    final Dialog builder = new Dialog(Vendedor.this);
                    builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    builder.setCancelable(false);

                    View dialoglayout = inflater.inflate(R.layout.informacion_vendedor, null);
                    builder.setContentView(dialoglayout);

                    //SPINNERS
                    horaInicioSpinner = dialoglayout.findViewById(R.id.spnInicioHora);
                    horaFinalSpinner = dialoglayout.findViewById(R.id.spnFinHora);
                    tiempoInicioSpinner = dialoglayout.findViewById(R.id.spnTiempoInicio);
                    tiempoFinalSpinner = dialoglayout.findViewById(R.id.spnTiempoFinal);
                    tipoVendedorSpinner = dialoglayout.findViewById(R.id.spnTipoVendedor);
                    //BOTON
                    btnGuardarInformacionVendedor= dialoglayout.findViewById(R.id.btnListo);
                    cerrarDialogo= dialoglayout.findViewById(R.id.cerrarDialogo);
                    //EDITTEXT
                    cajaDescripcion = dialoglayout.findViewById(R.id.txtDescripcionVenta);

                    cerrarDialogo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            builder.dismiss();
                        }
                    });

                    if(VENDEDOR.getDescripcion()!=null || VENDEDOR.getDescripcion()!=""){
                        cajaDescripcion.setText(VENDEDOR.getDescripcion());
                        horaInicioSpinner.setSelection(obtenerPosicionItem(horaInicioSpinner, VENDEDOR.getHoraIncio()));
                        tiempoInicioSpinner.setSelection(obtenerPosicionItem(tiempoInicioSpinner, VENDEDOR.getTiempoInicio()));
                        horaFinalSpinner.setSelection(obtenerPosicionItem(horaFinalSpinner, VENDEDOR.getHoraFinal()));
                        tiempoFinalSpinner.setSelection(obtenerPosicionItem(tiempoFinalSpinner, VENDEDOR.getTiempoFinal()));
                        tipoVendedorSpinner.setSelection(obtenerPosicionItem(tipoVendedorSpinner, VENDEDOR.getTipoVendedor()));
    
                    }
                    
                    btnGuardarInformacionVendedor.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {

                            if( VENDEDOR.getHoraIncio()==null&& VENDEDOR.getTiempoInicio()==null&& VENDEDOR.getHoraFinal()==null&&VENDEDOR.getTiempoFinal()==null&& VENDEDOR.getTipoVendedor()==null&&VENDEDOR.getDescripcion()==null&&VENDEDOR.getFotoUno()==null && VENDEDOR.getFotoDos()==null && VENDEDOR.getFotoTres()==null){
                                //SI ES LA PRIMERA VEZ EN REALIZAR REGISTRP

                                try {
                                    String h_inicio, t_inicio, h_final, t_final, tipo_vendedor, descripcionV;

                                    h_inicio = horaInicioSpinner.getSelectedItem().toString();
                                    t_inicio = tiempoInicioSpinner.getSelectedItem().toString();
                                    h_final = horaFinalSpinner.getSelectedItem().toString();
                                    t_final = tiempoFinalSpinner.getSelectedItem().toString();
                                    tipo_vendedor = tipoVendedorSpinner.getSelectedItem().toString();
                                    descripcionV = cajaDescripcion.getText().toString().replace(" ", "%20");

                                    if(h_inicio.equals("HORA:")||h_final.equals("HORA:")||t_inicio.equals("TIEMPO:")|| t_final.equals("TIEMPO:")||descripcionV.equals("") || tipo_vendedor.equals("CATEGORÍA:")){

                                        Toast.makeText(getApplicationContext(),"Falta algún campo por rellenar.", Toast.LENGTH_LONG).show();
                                    }else{

                                        registrarVendedor(h_inicio,t_inicio,h_final,t_final,tipo_vendedor,descripcionV,foto1,foto2,foto3,USUARIO.getCorreo(), USUARIO.getFoto(), USUARIO.getNombre(), USUARIO.getTelefono());

                                        builder.dismiss();

                                    }

                                }catch(Exception e){
                                    Toast.makeText(getApplicationContext(),"Debes subir tus fotos primero.", Toast.LENGTH_LONG).show();


                                }


                            }else{

                                //SI YA EXISTE SÓLO SE ACTUALIZARÁN LOS DATOS
                                
                            try {

                                    String h_inicio, t_inicio, h_final, t_final, tipo_vendedor, descripcionV;

                                    h_inicio = horaInicioSpinner.getSelectedItem().toString();
                                    t_inicio = tiempoInicioSpinner.getSelectedItem().toString();
                                    h_final = horaFinalSpinner.getSelectedItem().toString();
                                    t_final = tiempoFinalSpinner.getSelectedItem().toString();
                                    tipo_vendedor = tipoVendedorSpinner.getSelectedItem().toString();
                                    descripcionV = cajaDescripcion.getText().toString().replace(" ", "%20");

                                    if(h_inicio.equals("HORA:")||h_final.equals("HORA:")||t_inicio.equals("TIEMPO:")|| t_final.equals("TIEMPO:")||descripcionV.equals("") || tipo_vendedor.equals("CATEGORÍA:")){


                                        Toast.makeText(getApplicationContext(),"Falta algún campo por rellenar.", Toast.LENGTH_LONG).show();
                                    }else{

                                        actualizarInfo(h_inicio,t_inicio,h_final,t_final,tipo_vendedor,descripcionV,foto1,foto2,foto3,USUARIO.getCorreo());

                                        vistaDescripcion.setText(descripcionV);
                                        vistaTipoVendedor.setText(tipo_vendedor);
                                        vistaHorario.setText("DE   "+h_inicio+" "+t_inicio + "   A   " +h_final+" "+t_final);

                                        builder.dismiss();

                                    }

                                }catch(Exception e){
                                    Toast.makeText(getApplicationContext(),"Debes subir tus fotos primero.", Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    });


                    builder.show();

                }



            }
        });



        productoUno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.setType("image/jpeg");
                i.putExtra(Intent.EXTRA_LOCAL_ONLY,true);
                startActivityForResult(Intent.createChooser(i,"Selecciona una foto"),PHOTO_ONE);
            }
        });


        productoDos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.setType("image/jpeg");
                i.putExtra(Intent.EXTRA_LOCAL_ONLY,true);
                startActivityForResult(Intent.createChooser(i,"Selecciona una foto"),PHOTO_TWO);
            }
        });

        productoTres.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.setType("image/jpeg");
                i.putExtra(Intent.EXTRA_LOCAL_ONLY,true);
                startActivityForResult(Intent.createChooser(i,"Selecciona una foto"),PHOTO_TREE);
            }
        });


    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            Uri u = data.getData();

        Bitmap bitmap = decodificarBitmap(Vendedor.this, u);


        if (requestCode == PHOTO_ONE && resultCode == RESULT_OK) {
            subirFotoFirebase(u,PHOTO_ONE,bitmap);
            
        } else if (requestCode == PHOTO_TWO && resultCode == RESULT_OK) {
            subirFotoFirebase(u,PHOTO_TWO,bitmap);
            
        }else if (requestCode == PHOTO_TREE && resultCode == RESULT_OK) {
            subirFotoFirebase(u,PHOTO_TREE,bitmap);
        }

        } catch (Exception e) {
            Toast.makeText(this, "No se seleccionó foto", Toast.LENGTH_SHORT).show();
        }
        
    }
    
    
    //Método para obtener la posición de un ítem del spinner
    public static int obtenerPosicionItem(Spinner spinner, String fruta) {
        //Creamos la variable posicion y lo inicializamos en 0
        int posicion = 0;
        //Recorre el spinner en busca del ítem que coincida con el parametro `String fruta`
        //que lo pasaremos posteriormente
        for (int i = 0; i < spinner.getCount(); i++) {
            //Almacena la posición del ítem que coincida con la búsqueda
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(fruta)) {
                posicion = i;
            }
        }
        //Devuelve un valor entero (si encontro una coincidencia devuelve la
        // posición 0 o N, de lo contrario devuelve 0 = posición inicial)
        return posicion;
    }
    private void actualizarInfo(String h_inicio, String tiempo_inicio, String h_final, String tiempo_final, String tipo_vendedor, String descripcion, String foto_uno, String foto_dos, String foto_tres, String correo) {

        String url =     CONSTANTES.URL_PRINCIPAL+ "actualizar_info_vendedor.php?correo="+correo+"&hora_inicio="+h_inicio+"&t_incio="+tiempo_inicio+"&hora_final="+h_final+"&t_final="+tiempo_final+"&tipo_vendedor="+tipo_vendedor+"&descripcion="+descripcion+"&foto_uno="+(foto_uno.replace("%", "%25").replace("&", "%26"))+"&foto_dos="+(foto_dos.replace("%", "%25").replace("&", "%26"))+"&foto_tres="+(foto_tres.replace("%", "%25").replace("&", "%26"));


        stringRequest= new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Toast.makeText(getApplicationContext(),"Información actualizada!",Toast.LENGTH_LONG).show();

                vistaDescripcion.setText(cajaDescripcion.getText().toString());

                linearLottieAnim.setVisibility(View.VISIBLE);

                animacionLottie.setAnimation("tarea.json");
                animacionLottie.playAnimation();


                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        linearLottieAnim.setVisibility(View.GONE);

                    }
                }, 3000);



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                animacionLottie.setAnimation("error.json");
                animacionLottie.playAnimation();


                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        linearLottieAnim.setVisibility(View.GONE);

                    }
                }, 2000);

                Toast.makeText(getApplicationContext(),"Se produjo un problema al intentar actualizar.",Toast.LENGTH_LONG).show();

            }
        });

        requestQueue.add(stringRequest);


    }

  
    void subirFotoFirebase(Uri uri, final int numFoto, Bitmap bitmap){

        storageReference = storage.getReference("fotos_vendedores");//FOTOS SEGÚN SEA EL CASO
        final StorageReference fotoReferencia = storageReference.child(uri.getLastPathSegment());


        final byte[] data = getImageCompressed(bitmap);


        UploadTask uploadTask = fotoReferencia.putBytes(data);


        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();

                }

                return fotoReferencia.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                uriFinal=task.getResult().toString();

                if(numFoto==PHOTO_ONE){
                             //ELIMINO LA FOTO ANTERIOR DE FIREBASE
    
                    //CREO UN INSTANACIA DE LA BASE DE DATOS
                    SQLHelper conn = new SQLHelper(getApplicationContext(),SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);
    
                    //LA ESTABLESCO EN MODO LECTURA
                    SQLiteDatabase db=conn.getReadableDatabase();
    
                    //CREO UN CURSOR PARA RECORRER LOS CAMPOS
                    Cursor cursor = db.rawQuery("SELECT f1 from fproductos;", null);
    
                    //CREO VARIABLES PARA GUARDAR LO QUE ME RETORNE EL CURSOR SEGÚN EL TIPO DE VALOR O DATO
                    String nombre_id_foto_p1="";
    
                    while(cursor.moveToNext()){
                        //GUARDO LOS RESULTADOS EN LAS VARIABLES
                        nombre_id_foto_p1  = cursor.getString(0);
        
                    }
    
                    // CREO UNA REFERENCIA DEL STORAGE DE FIREBASE
                    StorageReference storageRef = storage.getReference();
    
                    //CREO LA REFERENCIA CON RUTA DE LA FOTO QUE ELIMINARÉ CONCATENANDO EL ID (NOMBRE DE LA FOTO EN FIREBASE) DE LA BD
                    StorageReference desertRef = storageRef.child("fotos_vendedores/"+nombre_id_foto_p1);
    
                    //LLAMO AL MÉTODO QUE ELIMINA LOS ARCHIVOS
                    desertRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
            
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
        
            
                        }
                    });
    
                    //PONGO LA BD EN MODO ESCRITURA
                    conn.getWritableDatabase();
    
                    //ACTUALIZO EL CAMPO DE LA LA TABLA CON EL NUEVO ID DE LA FOTO ACTUALIZADA
                    db.execSQL("UPDATE fproductos SET f1='"+fotoReferencia.getName()+"';");
        
                    
                    foto1=String.valueOf(uriFinal);
                    Glide.with(getApplicationContext()).load(uriFinal).into(productoUno);
    
                   
    
                }else if(numFoto==PHOTO_TWO){
                            //ELIMINO LA FOTO ANTERIOR DE FIREBASE
    
                    //CREO UN INSTANACIA DE LA BASE DE DATOS
                    SQLHelper conn = new SQLHelper(getApplicationContext(),SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);
    
                    //LA ESTABLESCO EN MODO LECTURA
                    SQLiteDatabase db=conn.getReadableDatabase();
    
                    //CREO UN CURSOR PARA RECORRER LOS CAMPOS
                    Cursor cursor = db.rawQuery("SELECT f2 from fproductos;", null);
    
                    //CREO VARIABLES PARA GUARDAR LO QUE ME RETORNE EL CURSOR SEGÚN EL TIPO DE VALOR O DATO
                    String nombre_id_foto_p2="";
    
                    while(cursor.moveToNext()){
                        //GUARDO LOS RESULTADOS EN LAS VARIABLES
                        nombre_id_foto_p2  = cursor.getString(0);
        
                    }
    
                    // CREO UNA REFERENCIA DEL STORAGE DE FIREBASE
                    StorageReference storageRef = storage.getReference();
    
                    //CREO LA REFERENCIA CON RUTA DE LA FOTO QUE ELIMINARÉ CONCATENANDO EL ID (NOMBRE DE LA FOTO EN FIREBASE) DE LA BD
                    StorageReference desertRef = storageRef.child("fotos_vendedores/"+nombre_id_foto_p2);
    
                    //LLAMO AL MÉTODO QUE ELIMINA LOS ARCHIVOS
                    desertRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
            
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
            
            
                        }
                    });
    
                    //PONGO LA BD EN MODO ESCRITURA
                    conn.getWritableDatabase();
    
                    //ACTUALIZO EL CAMPO DE LA LA TABLA CON EL NUEVO ID DE LA FOTO ACTUALIZADA
                    db.execSQL("UPDATE fproductos SET f2='"+fotoReferencia.getName()+"';");
    
                    
                    foto2=String.valueOf(uriFinal);
                    Glide.with(getApplicationContext()).load(uriFinal).into(productoDos);
    
                   

                }else if(numFoto==PHOTO_TREE){
                    //ELIMINO LA FOTO ANTERIOR DE FIREBASE
    
                    //CREO UN INSTANACIA DE LA BASE DE DATOS
                    SQLHelper conn = new SQLHelper(getApplicationContext(),SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);
    
                    //LA ESTABLESCO EN MODO LECTURA
                    SQLiteDatabase db=conn.getReadableDatabase();
    
                    //CREO UN CURSOR PARA RECORRER LOS CAMPOS
                    Cursor cursor = db.rawQuery("SELECT f3 from fproductos;", null);
    
                    //CREO VARIABLES PARA GUARDAR LO QUE ME RETORNE EL CURSOR SEGÚN EL TIPO DE VALOR O DATO
                    String nombre_id_foto_p3="";
    
                    while(cursor.moveToNext()){
                        //GUARDO LOS RESULTADOS EN LAS VARIABLES
                        nombre_id_foto_p3  = cursor.getString(0);
        
                    }
    
                    // CREO UNA REFERENCIA DEL STORAGE DE FIREBASE
                    StorageReference storageRef = storage.getReference();
    
                    //CREO LA REFERENCIA CON RUTA DE LA FOTO QUE ELIMINARÉ CONCATENANDO EL ID (NOMBRE DE LA FOTO EN FIREBASE) DE LA BD
                    StorageReference desertRef = storageRef.child("fotos_vendedores/"+nombre_id_foto_p3);
    
                    //LLAMO AL MÉTODO QUE ELIMINA LOS ARCHIVOS
                    desertRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
            
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
            
            
                        }
                    });
    
                    //PONGO LA BD EN MODO ESCRITURA
                    conn.getWritableDatabase();
    
                    //ACTUALIZO EL CAMPO DE LA LA TABLA CON EL NUEVO ID DE LA FOTO ACTUALIZADA
                    db.execSQL("UPDATE fproductos SET f3='"+fotoReferencia.getName()+"';");
    
      
    
                    foto3=String.valueOf(uriFinal);
                    Glide.with(getApplicationContext()).load(uriFinal).into(productoTres);
    
                }

                progressDialog.hide();
                Toast.makeText(getApplicationContext(),"Foto cargada!", Toast.LENGTH_LONG).show();


            }
        });

        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                double cargado = (taskSnapshot.getBytesTransferred()/1024);
                double total = (taskSnapshot.getTotalByteCount()/1024);
                int cargaActual = (int) cargado;


                progressDialog.setTitle("Subiendo tu foto, aguanta un rato");
                progressDialog.setMessage(((int)progress) + "% subido...");
                progressDialog.setCancelable(false);
                progressDialog.setProgressStyle(2);
                progressDialog.show();


            }
        });




    }


    public void  registrarVendedor(String h_inicio, String tiempo_inicio, String h_final, String tiempo_final, String tipo_vendedor, String descripcion, String foto_uno, String foto_dos, String foto_tres, String correo, String fotoPerfilUsuario, String usuarioNombre, String telefonoUsuario){


       String url =     CONSTANTES.URL_PRINCIPAL+ "insertar_v.php?hora_inicio="+h_inicio+"&t_incio="+tiempo_inicio+"&hora_final="+h_final+"&t_final="+tiempo_final+"&tipo_vendedor="+tipo_vendedor+"&descripcion="+descripcion+"&foto_uno="+(foto_uno.replace("%", "%25").replace("&", "%26"))+"&foto_dos="+(foto_dos.replace("%", "%25").replace("&", "%26"))+"&foto_tres="+(foto_tres.replace("%", "%25").replace("&", "%26"))+"&correo="+correo+"&foto_perfil="+(fotoPerfilUsuario.replace("%", "%25").replace("&", "%26"))+"&usuario_nombre="+usuarioNombre+"&telefono_usuario="+telefonoUsuario;

        stringRequest= new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
    
                Toast.makeText(getApplicationContext(),"Información publicada!",Toast.LENGTH_LONG).show();

                VENDEDOR.setDescripcion(cajaDescripcion.getText().toString());
                
                buscarVendedor(USUARIO.getCorreo());

                vistaHorario.setText("DE   "+VENDEDOR.getHoraIncio()+" "+VENDEDOR.getTiempoInicio() + "   A   " +VENDEDOR.getHoraFinal()+" "+VENDEDOR.getTiempoFinal());
                vistaTipoVendedor.setText(VENDEDOR.getTipoVendedor());
                vistaDescripcion.setText(VENDEDOR.getDescripcion());

                animacionLottie.setAnimation("done.json");
                animacionLottie.playAnimation();


                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                         linearLottieAnim.setVisibility(View.GONE);

                    }
                }, 2000);




            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getApplicationContext(),"ERROR! "+error.toString(),Toast.LENGTH_LONG).show();

            }
        });

        requestQueue.add(stringRequest);

    }

    void inicializar(){

        //Incializar animaciones
        smalltobig = AnimationUtils.loadAnimation(this, R.anim.smalltobig);
        btta = AnimationUtils.loadAnimation(this, R.anim.btta);
        btta2 = AnimationUtils.loadAnimation(this, R.anim.btta2);
        atg = AnimationUtils.loadAnimation(this, R.anim.atgthree);
        packageimg = AnimationUtils.loadAnimation(this, R.anim.packageimg);
        bganim = AnimationUtils.loadAnimation(this, R.anim.clovernim);


        //IMAGEN REDONDA CASTING
        imagenVendedor= findViewById(R.id.circuloFotoVendedor);
        //TEXTVIEWS
        vistaNombre= findViewById(R.id.tvNombreVendedor);
        vistaHorario= findViewById(R.id.tvHorario);
        vistaDescripcion= findViewById(R.id.tvDescripcionVendedor);
        vistaTipoVendedor= findViewById(R.id.tvTipoVendedor);
        progressDialog = new ProgressDialog(this);

        //BOTÓN FLOTANTE
        botonEditarInformacion = findViewById(R.id.botonEditarInfo);

        //IMÁGENES DE LOS PRODUCTOS
        productoUno = findViewById(R.id.imgProductoUno);
        productoDos = findViewById(R.id.imgProductoDos);
        productoTres = findViewById(R.id.imgProductoTres);

        //CARGO LA URL DEL USUARIO A LA IMAGEN REDODNDA
        Glide.with(getApplicationContext()).load(Uri.parse(USUARIO.getFoto())).into(imagenVendedor);
        //CARGO EL NOMBRE AL VENDEDOR TRAIDO DEL OBJETO USUARIO
        vistaNombre.setText(USUARIO.getNombre());

        //INCIALIZACIÓN WEBSERVICE
        requestQueue = Volley.newRequestQueue(this);

        //fotos
        storage = FirebaseStorage.getInstance();

        //LOTTIE
        animacionLottie = findViewById(R.id.lottieVendedor);
        linearLottieAnim = findViewById(R.id.linearLottie);

        linearBase  = findViewById(R.id.linearBase);
        informacionTarjeta = findViewById(R.id.tarjetaInfo);
        tituloN = findViewById(R.id.tvNombreVende);
        tituloH = findViewById(R.id.tvHorarioTitle);
        tituloDescrip = findViewById(R.id.tvDescripTitle);
        tituloTipoV = findViewById(R.id.tvTipoTitle);
        tituloFotitos  = findViewById(R.id.tvFotitosProductos);


       // linearBase.startAnimation(bganim);
       // linearBase.startAnimation(smalltobig);
        productoUno.startAnimation(btta);
        productoDos.startAnimation(btta);
        productoTres.startAnimation(btta);
        imagenVendedor.startAnimation(packageimg);

        informacionTarjeta.startAnimation(btta2);
        botonEditarInformacion.startAnimation(smalltobig);


        examinarTema();
    }


    public String verificarConteoBD(){
        String valor="";

//CREO UN INSTANACIA DE LA BASE DE DATOS
        SQLHelper conn = new SQLHelper(Vendedor.this,SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);

        //LA ESTABLESCO EN MODO LECTURA
        SQLiteDatabase db=conn.getReadableDatabase();

        //CREO UN CURSOR PARA RECORRER LOS CAMPOS
        Cursor cursor = db.rawQuery("SELECT cuenta from contador;", null);

        //CREO VARIABLES PARA GUARDAR LO QUE ME RETORNE EL CURSOR SEGÚN EL TIPO DE VALOR O DATO
        String estado="";

        while(cursor.moveToNext()){

            //GUARDO LOS RESULTADOS EN LAS VARIABLES
            estado  = cursor.getString(0);

            if(estado.equals("1")){

                valor="1";
            }else{

                valor="2";

            }


        }

        return valor;

    }


    void buscarVendedor(String correo){
        final String url =    CONSTANTES.URL_PRINCIPAL+ "traer_info_vendedor.php?correo="+correo;

        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                JSONArray json = response.optJSONArray("vendedor");
                JSONObject jsonObject = null;

                try {
                    jsonObject=json.getJSONObject(0);
                    VENDEDOR.setHoraIncio(jsonObject.optString("hora_inicio"));
                    VENDEDOR.setTiempoInicio(jsonObject.optString("t_incio"));
                    VENDEDOR.setHoraFinal(jsonObject.optString("hora_final"));
                    VENDEDOR.setTiempoFinal(jsonObject.optString("t_final"));
                    VENDEDOR.setTipoVendedor(jsonObject.optString("tipo_vendedor"));
                    VENDEDOR.setDescripcion(jsonObject.optString("descripcion"));
                    VENDEDOR.setFotoUno(jsonObject.optString("foto_uno"));
                    VENDEDOR.setFotoDos(jsonObject.optString("foto_dos"));
                    VENDEDOR.setFotoTres(jsonObject.optString("foto_tres"));
                    VENDEDOR.setCorreo(jsonObject.optString("correo"));

                    VENDEDOR.setFotoPerfilUsuario(jsonObject.optString("foto_perfil"));
                    VENDEDOR.setNombreUsuario(jsonObject.optString("usuario_nombre"));
                    VENDEDOR.setTelefonoUsuario(jsonObject.optString("telefono_usuario"));


                    ///un mensaje aleatorio agregaré aquí hehe



                    vistaHorario.setText("DE   "+VENDEDOR.getHoraIncio()+" "+VENDEDOR.getTiempoInicio() + "   A   " +VENDEDOR.getHoraFinal()+" "+VENDEDOR.getTiempoFinal());
                    vistaTipoVendedor.setText(VENDEDOR.getTipoVendedor());
                    vistaDescripcion.setText(VENDEDOR.getDescripcion());
                    Glide.with(getApplicationContext()).load(Uri.parse(VENDEDOR.getFotoUno())).into(productoUno);
                    Glide.with(getApplicationContext()).load(Uri.parse(VENDEDOR.getFotoDos())).into(productoDos);
                    Glide.with(getApplicationContext()).load(Uri.parse(VENDEDOR.getFotoTres())).into(productoTres);
                    foto1= VENDEDOR.getFotoUno();
                    foto2= VENDEDOR.getFotoDos();
                    foto3 = VENDEDOR.getFotoTres();


                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(),"Hubo un error al cargar la información. ",Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getApplicationContext(),"Registra tu información =)" ,Toast.LENGTH_LONG).show();


            }
        });

        requestQueue.add(jsonObjectRequest);

        ////

    }

    private byte[] getImageCompressed(Bitmap photo) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.JPEG, 70, baos);



        return baos.toByteArray();
    }

    private Bitmap decodificarBitmap(Context ctx, Uri uri) throws FileNotFoundException {
        int anchoMarco = 600;
        int altoMarco = 600;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
        int fotoAncho = bmOptions.outWidth;
        int fotoAlto = bmOptions.outHeight;

        int escalaImagen = Math.min(fotoAncho / anchoMarco, fotoAlto / altoMarco);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = escalaImagen;

        return BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
    }

    private void examinarTema() {
        //CREO UN INSTANACIA DE LA BASE DE DATOS
        SQLHelper conn = new SQLHelper(Vendedor.this,SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);

        //LA ESTABLESCO EN MODO LECTURA
        SQLiteDatabase db=conn.getReadableDatabase();

        //CREO UN CURSOR PARA RECORRER LOS CAMPOS
        Cursor cursor = db.rawQuery("SELECT estado from tema;", null);

        //CREO VARIABLES PARA GUARDAR LO QUE ME RETORNE EL CURSOR SEGÚN EL TIPO DE VALOR O DATO
        String estado="";

        while(cursor.moveToNext()){

            //GUARDO LOS RESULTADOS EN LAS VARIABLES
            estado  = cursor.getString(0);

            if(estado.equals("true")){
                //TEMA BLACK
                linearBase.setBackgroundColor(Color.parseColor("#232323"));

                    informacionTarjeta. setCardBackgroundColor(Color.parseColor("#1F1E1E"));
                    tituloFotitos.setTextColor(Color.parseColor("#F4D121"));
                    tituloTipoV.setTextColor(Color.parseColor("#F4D121"));
                    tituloH.setTextColor(Color.parseColor("#F4D121"));
                    tituloDescrip.setTextColor(Color.parseColor("#F4D121"));
                    tituloN.setTextColor(Color.parseColor("#F4D121"));

                    vistaNombre.setTextColor(Color.WHITE);
                    vistaTipoVendedor.setTextColor(Color.WHITE);
                    vistaDescripcion.setTextColor(Color.WHITE);
                    vistaHorario.setTextColor(Color.WHITE);
                TextView t = findViewById(R.id.textitoVendedor);
                t.setTextColor(Color.YELLOW);


            }else{
                //VALORES ORIGINALES


            }


        }


    }
}
