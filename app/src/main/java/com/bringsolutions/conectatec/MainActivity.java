package com.bringsolutions.conectatec;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import static com.bringsolutions.conectatec.objetos.CONSTANTES.USUARIO;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bringsolutions.conectatec.BD.SQLHelper;
import com.bringsolutions.conectatec.FragmentsNavegacion.Avisos;
import com.bringsolutions.conectatec.FragmentsNavegacion.CalendarioITVH;
import com.bringsolutions.conectatec.FragmentsNavegacion.ChaTec;
import com.bringsolutions.conectatec.FragmentsNavegacion.Complementarias;
import com.bringsolutions.conectatec.FragmentsNavegacion.Ingles;
import com.bringsolutions.conectatec.FragmentsNavegacion.SIE;
import com.bringsolutions.conectatec.FragmentsNavegacion.SWS;
import com.bringsolutions.conectatec.FragmentsNavegacion.SpeakTec;
import com.bringsolutions.conectatec.FragmentsNavegacion.TipoLocaTec;
import com.bringsolutions.conectatec.FragmentsNavegacion.Vendedores;
import com.bringsolutions.conectatec.objetos.CONSTANTES;
import com.bringsolutions.conectatec.para.vendedores.Vendedor;
import com.bumptech.glide.Glide;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;

import de.hdodenhof.circleimageview.CircleImageView;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    ImageView imagenPerfil;
    TextView nombrePerfil, tipoPerfil;
    View bannerHead;
    NavigationView navigationView;
    ProgressDialog progressDialog;
    Button perfilVendedor;

    //constantes
    final int PHOTO_PERFIL=1;

    //mensajes
    private FirebaseDatabase database;
    private DatabaseReference databaseReference;

    //fotoa
    private FirebaseStorage storage;
    private StorageReference storageReference;

    //WebService
    RequestQueue requestQueue;

    JsonObjectRequest jsonObjectRequest;

    int caso;

  

    SwitchCompat swichtSesion,swichtTema;
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        inicializarComponentes();

        clicksEventos();


        buscarFoto(USUARIO.getCorreo());

    if(primerUso(this)==0){
        final CircleImageView imagenCat = findViewById(R.id.fotitoGato);
        imagenCat.setVisibility(View.VISIBLE);

        //CREO UNA TARJETA DE BIENVENIDA, DEL PRIMER USO
        TapTargetView.showFor(this, TapTarget.forView(findViewById(R.id.fotitoGato), "¡Bienvenido a ConectaTec!", "Aplicación desarrollada por un estudiante para estudiantes. Estamos en constantes cambios, por lo que me interesaría saber tus sugerencias y/u opiniones para mejorar la aplicación y sus servicios.     ")
                .outerCircleAlpha(0.96f)            // Specify the alpha amount for the outer circle
                  // Specify a color for the target circle
                .titleTextSize(20)           // Specify the size (in sp) of the title text
                .titleTextColor(R.color.textoblanco)      // Specify the color of the title text
                .descriptionTextSize(15)  .descriptionTextAlpha(0.85f)          // Specify the size (in sp) of the description text
                .descriptionTextColor(R.color.textoblanco)  // Specify the color of the description text
                .textColor(R.color.textoblanco)            // Specify a color for both the title and description text
                .textTypeface(Typeface.SANS_SERIF)
                .dimColor(R.color.textoblanco)
                .drawShadow(true)
                .cancelable(false)
                .tintTarget(false)
                .transparentTarget(false)

                .targetRadius(35)     );

        //ESPERO UN SEGUNDO Y MEDIO Y MANDO A LA ACTIVIDAD PRINCIPAL
        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            public void run() {

                imagenCat.setVisibility(View.GONE);
            }
        }, 1500);

        //CREO UN INSTANACIA DE LA BASE DE DATOS
        SQLHelper conn = new SQLHelper(getApplicationContext(),SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);
        //LA ABRO EN MODO ESCRITURA
        SQLiteDatabase db=conn.getWritableDatabase();
        //INSERTO EN LA TABLA USO QUE ES LA PRIMERA VEZ QUE SE USA LA APP
        db.execSQL("INSERT INTO uso(estado) values ('true')");

    }else if(primerUso(this)==1){
        //CREO UN INSTANACIA DE LA BASE DE DATOS
        SQLHelper conn = new SQLHelper(getApplicationContext(),SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);
        //LA ABRO EN MODO ESCRITURA
        SQLiteDatabase db=conn.getWritableDatabase();
        //ACTUALIZO LA TABLA USO PARA ESTABLECER QUE NO ES LA PRIMERA VEZ QUE SE USA LA APP
        db.execSQL("UPDATE uso SET estado='false'");
    }

    Menu m = navigationView.getMenu();
    
    MenuItem menuItemSWsesion =m.findItem(R.id.sw_sesion_item);
    MenuItem menuItemSWtema =m.findItem(R.id.sw_tema_black_item);

    View actionViewSWtema = MenuItemCompat.getActionView(menuItemSWtema);
    View actionViewSWsesion = MenuItemCompat.getActionView(menuItemSWsesion);

    swichtSesion =(SwitchCompat) actionViewSWsesion.findViewById(R.id.sw_sesion);
    swichtTema=(SwitchCompat) actionViewSWtema.findViewById(R.id.sw_tema);

    verificarEstadoTema();

    verificarEstadoSesion();

    swichtSesion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            //CREO UN INSTANACIA DE LA BASE DE DATOS
            SQLHelper conn = new SQLHelper(getApplicationContext(),SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);
            //LA ABRO EN MODO ESCRITURA
            SQLiteDatabase db=conn.getWritableDatabase();
            
            if(isChecked){ //ACTIVAR SWITCH
                // ACTUALIZO LOS CAMPOS CON LOS NUEVOS DATOS DE LOS EDITTEX
                db.execSQL("UPDATE recordarpass SET estado='true', correo= '"+USUARIO.getCorreo()+"', pass='"+USUARIO.getPass()+"';");
                //LANZO UN MENSAJE DE AVISO
                Toast.makeText(getApplicationContext(),"Activado" , Toast.LENGTH_SHORT).show();
                
            }else { //DESACTIVAR SWITCH
                //VUELVO A LOS VALORES ORIGINALES PARA CADA CAMPO
                db.execSQL("UPDATE recordarpass SET estado='false',correo ='sincorreo', pass='sinpass';");
                //LANZO UN MENSAJE DE AVISO
                Toast.makeText(getApplicationContext(),"Desactivado" , Toast.LENGTH_SHORT).show();
            }
        }
    });

        swichtTema.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                
                if(isChecked){ //ACTIVAR TEMA OSCURO
                    
                    // PONGO OSCURO EL FONDO DEL MENÚ
                    navigationView.setBackgroundColor(Color.parseColor("#262626"));

                    //CREO MI ARREGLO ESTADOS DE ACCIONES
                    int[][] states = new int[][] {
                            new int[] { android.R.attr.state_enabled}, // enabled
                            new int[] {-android.R.attr.state_selected}, // disabled
                            new int[] {-android.R.attr.state_checked}, // unchecked
                            new int[] { android.R.attr.state_pressed}  // pressed
                    };

                    //CREO MI ARREGLO DE COLORES
                    int[] colors = new int[] {
                            Color.parseColor("#BDC3C7"),
                            Color.RED,
                            Color.GREEN,
                            Color.BLUE
                    };
                    
                    //CREO OBJETO DE LISTA DE COLORES Y LE PASO MIS ARREGLOS
                    ColorStateList myList = new ColorStateList(states, colors);

                    //LE PASO LA LISTA DE COLORES A LOS ITEMS DEL MENÚ
                    navigationView.setItemTextColor(myList);

                    //CREO MI ARREGLO DE ESTADO DE ACCIONES PARA LOS ITEMS DEL MENÚ
                    int[][] statesItem = new int[][] {
                            new int[] { android.R.attr.state_enabled}, // enabled
                            new int[] {-android.R.attr.state_selected}, // disabled
                            new int[] {-android.R.attr.state_checked}, // unchecked
                            new int[] { android.R.attr.state_pressed}  // pressed
                    };

                    //CREO LOS COLORES PARA LOS ITEMS
                    int[] colorsItem = new int[] {
                            Color.parseColor("#F4D121"),
                            Color.RED,
                            Color.GREEN,
                            Color.BLUE
                    };
                    
                    //CREO MI OBJETO DE LISTA DE COLORES PARA LOS ITEMS
                    ColorStateList myListItem = new ColorStateList(statesItem, colorsItem);
                    
                    //AGREGO LA LISTA DE COLORES A LOS ITEMS DEL MENÚ
                    navigationView.setItemIconTintList(myListItem);

                    toolbar.setBackgroundColor(Color.parseColor("#191919"));

                    //CREO UN INSTANACIA DE LA BASE DE DATOS
                    SQLHelper conn = new SQLHelper(getApplicationContext(),SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);
                    //LA ABRO EN MODO ESCRITURA
                    SQLiteDatabase db=conn.getWritableDatabase();

                    //ACTUALIZO EL CAMPO DE LA LA TABLA
                    db.execSQL("UPDATE tema SET estado='true';");

                    Toast.makeText(getApplicationContext(),"Activado" , Toast.LENGTH_SHORT).show();

                    FragmentManager fm = getFragmentManager();
                    fm.beginTransaction().replace(R.id.contendorFragments, new ChaTec()).addToBackStack(null).commit();

                }else { //ACTIVAR TEMA CLARO
                    navigationView.setBackgroundColor(Color.WHITE);


                    int[][] states = new int[][] {
                            new int[] { android.R.attr.state_enabled}, // enabled
                            new int[] {-android.R.attr.state_selected}, // disabled
                            new int[] {-android.R.attr.state_checked}, // unchecked
                            new int[] { android.R.attr.state_pressed}  // pressed
                    };

                    int[] colors = new int[] {
                            Color.parseColor("#000000"),
                            Color.RED,
                            Color.GREEN,
                            Color.BLUE
                    };

                    ColorStateList myList = new ColorStateList(states, colors);

                    navigationView.setItemTextColor(myList);


                    int[][] statesItem = new int[][] {
                            new int[] { android.R.attr.state_enabled}, // enabled
                            new int[] {-android.R.attr.state_selected}, // disabled
                            new int[] {-android.R.attr.state_checked}, // unchecked
                            new int[] { android.R.attr.state_pressed}  // pressed
                    };

                    int[] colorsItem = new int[] {
                            Color.parseColor("#00574B"),
                            Color.RED,
                            Color.GREEN,
                            Color.BLUE
                    };

                    ColorStateList myListItem = new ColorStateList(statesItem, colorsItem);

                    navigationView.setItemIconTintList(myListItem);

                    //CREO UN INSTANACIA DE LA BASE DE DATOS
                    SQLHelper conn = new SQLHelper(getApplicationContext(),SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);
                    //LA ABRO EN MODO ESCRITURA
                    SQLiteDatabase db=conn.getWritableDatabase();
                    //ACTUALIZO EL CAMPO DE LA LA TABLA
                    db.execSQL("UPDATE tema SET estado='false';");

                    toolbar.setBackgroundColor(Color.parseColor("#5ebe30"));

                    FragmentManager fm = getFragmentManager();
                    fm.beginTransaction().replace(R.id.contendorFragments, new ChaTec()).addToBackStack(null).commit();


                    Toast.makeText(getApplicationContext(),"Desactivado" , Toast.LENGTH_SHORT).show();

                }
            }
        });


    }

    private void verificarEstadoTema() {

        //CREO UN INSTANACIA DE LA BASE DE DATOS
        SQLHelper conn = new SQLHelper(getApplicationContext(),SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);

        //LA ESTABLESCO EN MODO LECTURA
        SQLiteDatabase db=conn.getReadableDatabase();

        //CREO UN CURSOR PARA RECORRER LOS CAMPOS
        Cursor cursor = db.rawQuery("SELECT estado from tema;", null);

        //CREO VARIABLES PARA GUARDAR LO QUE ME RETORNE EL CURSOR SEGÚN EL TIPO DE VALOR O DATO
        String estado="";

        while(cursor.moveToNext()){
            //GUARDO LOS RESULTADOS EN LAS VARIABLES
            estado  = cursor.getString(0);

            if(estado.equals("true")){ //TEMS COLOR OSCURO ACTIVADO
                swichtTema.setChecked(true);
                navigationView.setBackgroundColor(Color.parseColor("#262626"));

                int[][] states = new int[][] {
                        new int[] { android.R.attr.state_enabled}, // enabled
                        new int[] {-android.R.attr.state_selected}, // disabled
                        new int[] {-android.R.attr.state_checked}, // unchecked
                        new int[] { android.R.attr.state_pressed}  // pressed
                };

                int[] colors = new int[] {
                        Color.parseColor("#F4D121"),
                        Color.RED,
                        Color.GREEN,
                        Color.BLUE
                };

                ColorStateList myList = new ColorStateList(states, colors);

                navigationView.setItemTextColor(myList);

                int[][] statesItem = new int[][] {
                        new int[] { android.R.attr.state_enabled}, // enabled
                        new int[] {-android.R.attr.state_selected}, // disabled
                        new int[] {-android.R.attr.state_checked}, // unchecked
                        new int[] { android.R.attr.state_pressed}  // pressed
                };

                int[] colorsItem = new int[] {
                        Color.parseColor("#CDCDCD"),
                        Color.RED,
                        Color.GREEN,
                        Color.BLUE
                };

                ColorStateList myListItem = new ColorStateList(statesItem, colorsItem);

                navigationView.setItemIconTintList(myListItem);

                toolbar.setBackgroundColor(Color.parseColor("#191919"));


            }else{ //SE ACTIVA EL TEMA CLARO
                swichtTema.setChecked(false);
                
                navigationView.setBackgroundColor(Color.WHITE);

                int[][] states = new int[][] {
                        new int[] { android.R.attr.state_enabled}, // enabled
                        new int[] {-android.R.attr.state_selected}, // disabled
                        new int[] {-android.R.attr.state_checked}, // unchecked
                        new int[] { android.R.attr.state_pressed}  // pressed
                };

                int[] colors = new int[] {
                        Color.parseColor("#000000"),
                        Color.RED,
                        Color.GREEN,
                        Color.BLUE
                };

                ColorStateList myList = new ColorStateList(states, colors);

                navigationView.setItemTextColor(myList);
                
                int[][] statesItem = new int[][] {
                        new int[] { android.R.attr.state_enabled}, // enabled
                        new int[] {-android.R.attr.state_selected}, // disabled
                        new int[] {-android.R.attr.state_checked}, // unchecked
                        new int[] { android.R.attr.state_pressed}  // pressed
                };

                int[] colorsItem = new int[] {
                        Color.parseColor("#00574B"),
                        Color.RED,
                        Color.GREEN,
                        Color.BLUE
                };

                ColorStateList myListItem = new ColorStateList(statesItem, colorsItem);

                navigationView.setItemIconTintList(myListItem);

                toolbar.setBackgroundColor(Color.parseColor("#5ebe30"));

            }
        }
    }


    void verificarEstadoSesion(){

        //CREO UN INSTANACIA DE LA BASE DE DATOS
        SQLHelper conn = new SQLHelper(getApplicationContext(),SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);

        //LA ESTABLESCO EN MODO LECTURA
        SQLiteDatabase db=conn.getReadableDatabase();

        //CREO UN CURSOR PARA RECORRER LOS CAMPOS
        Cursor cursor = db.rawQuery("SELECT estado from recordarpass;", null);

        //CREO VARIABLES PARA GUARDAR LO QUE ME RETORNE EL CURSOR SEGÚN EL TIPO DE VALOR O DATO
        String estado="";

        while(cursor.moveToNext()){

            //GUARDO LOS RESULTADOS EN LAS VARIABLES
            estado  = cursor.getString(0);

            if(estado.equals("true")){
                swichtSesion.setChecked(true);
            }else{
                swichtSesion.setChecked(false);

            }
        }
    }

    //Retorna: 0 primera vez / 1 no es primera vez / 2 nueva versión
    public static int primerUso(Context contexto) {
        SharedPreferences sp = contexto.getSharedPreferences("ConectaTec", 0);
        int result, currentVersionCode = BuildConfig.VERSION_CODE;
        int lastVersionCode = sp.getInt("FIRSTTIMERUN", -1);
        if (lastVersionCode == -1) result = 0; else
            result = (lastVersionCode == currentVersionCode) ? 1 : 2;
        sp.edit().putInt("FIRSTTIMERUN", currentVersionCode).apply();
        return result;
    }

    public void clicksEventos(){

        imagenPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(USUARIO.getFoto()==null || USUARIO.getFoto()==""){
                    caso=1;
                    Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                    i.setType("image/jpeg");
                    i.putExtra(Intent.EXTRA_LOCAL_ONLY,true);
                    startActivityForResult(Intent.createChooser(i,"Selecciona una foto"),PHOTO_PERFIL);

                }else{

                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("CAMBIAR FOTO DE PERFIL").setIcon(R.drawable.icoct)
                            .setMessage("¿Quieres actualizar tu foto de perfil?")

                            .setPositiveButton("SÍ",
                                    new DialogInterface.OnClickListener() {
                                        @TargetApi(11)
                                        public void onClick(DialogInterface dialog, int id) {
                                            caso=2;
                                            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                                            i.setType("image/jpeg");
                                            i.putExtra(Intent.EXTRA_LOCAL_ONLY,true);
                                            startActivityForResult(Intent.createChooser(i,"Selecciona una foto"),PHOTO_PERFIL);
                                            dialog.cancel();
                                        }
                                    })
                            .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                                @TargetApi(11)
                                public void onClick(DialogInterface dialog, int id) {

                                    //SÓLO CERRAR
                                    dialog.cancel();
                                }
                            }).show();

                }
            }
        });

        perfilVendedor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Vendedor.class));
            }
        });


    }


    void inicializarComponentes(){
        //getSupportFragmentManager().beginTransaction().replace(frameCont,new ChaTec()).commit();

        bannerHead  = navigationView.getHeaderView(0);
        imagenPerfil = bannerHead.findViewById(R.id.imagenPerfil);
        nombrePerfil = bannerHead.findViewById(R.id.nombePerfil);
        tipoPerfil = bannerHead.findViewById(R.id.tipoPerfil);
        perfilVendedor = bannerHead.findViewById(R.id.btnPerfilVendedor);

        progressDialog = new ProgressDialog(this);

        //CARGO NOMBRE Y TIPO DE USUARIO DESDE LA CLASE USUARIO

        nombrePerfil.setText(USUARIO.getNombre());

        if(Integer.parseInt(USUARIO.getTipo())==2){

            tipoPerfil.setText("Vendedor");
            perfilVendedor.setVisibility(View.VISIBLE);
            
        }else if(Integer.parseInt(USUARIO.getTipo())==1){
            tipoPerfil.setText("Estudiante");
        }

        //mensajes
        database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference("mensajes");//Sala de chat (nombre)

        //fotos
        storage = FirebaseStorage.getInstance();

        //Volley
        requestQueue = Volley.newRequestQueue(this);
    }

    public  Bitmap decodificarBitmap(Context ctx, Uri uri) throws FileNotFoundException {
        int anchoMarco = 600;
        int altoMarco = 600;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
        int fotoAncho = bmOptions.outWidth;
        int fotoAlto = bmOptions.outHeight;

        int escalaImagen = Math.min(fotoAncho / anchoMarco, fotoAlto / altoMarco);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = escalaImagen;

        return BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
        Uri u = data.getData(); //Datos de la imagen seleccionada

        Bitmap bitmap = decodificarBitmap(MainActivity.this, u);

        //caso 1 = subir primera vez
        if(requestCode == PHOTO_PERFIL && resultCode == RESULT_OK &&caso==1){

              //se sube a firebase
                subirFotoPerfil(u,PHOTO_PERFIL,bitmap);


        //caso 2  = actualizar la foto (se subirá otra foto pero se mantiene en la nube la anterior)
        }else if(requestCode == PHOTO_PERFIL && resultCode == RESULT_OK &&caso==2){
           
            //se sube a firebase
               subirFotoPerfil(u,PHOTO_PERFIL,bitmap);

        }


    } catch (Exception e) {
            Toast.makeText(this, "No se seleccionó foto", Toast.LENGTH_SHORT).show();
    }

    }

    private byte[] getImageCompressed(Bitmap photo) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.JPEG, 70, baos);



        return baos.toByteArray();
    }

    //MÉTODO QUE SUBE LA FOTO A FIREBASE STORAGE 1=PERFIL 2= FOTOS CHAT
    private void subirFotoPerfil(final Uri uriFoto, final int tipoFoto, Bitmap bitmap) {
        String referencia="";
        if(tipoFoto==1){
            referencia="fotos_perfil";

        }else if (tipoFoto==2){
            referencia="fotos_chat";

        }

        storageReference = storage.getReference(referencia);//FOTOS SEGÚN SEA EL CASO
        
        
        final StorageReference fotoReferencia = storageReference.child(uriFoto.getLastPathSegment());

        final byte[] data = getImageCompressed(bitmap);


        UploadTask uploadTask = fotoReferencia.putBytes(data);



        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }

                return fotoReferencia.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                progressDialog.hide();

                if(tipoFoto==1){
                    Glide.with(getApplicationContext()).load(Uri.parse(task.getResult().toString())).into(imagenPerfil);

                }

                    if(caso==1){
                        //SE SUBE FOTO POR PRIMERA VEZ
                        cargarWebService(USUARIO.getCorreo(),task.getResult());

                        USUARIO.setFoto(task.getResult().toString());
    
                        //CREO UN INSTANACIA DE LA BASE DE DATOS
                        SQLHelper conn = new SQLHelper(getApplicationContext(),SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);
                        //LA ABRO EN MODO ESCRITURA
                        SQLiteDatabase db=conn.getWritableDatabase();
                        //ACTUALIZO LA TABLA USO PARA ESTABLECER QUE NO ES LA PRIMERA VEZ QUE SE USA LA APP
                        db.execSQL("UPDATE fotoperfil SET nombre='"+fotoReferencia.getName()+"';");
                        
                        FragmentManager fm = getFragmentManager();
                        fm.beginTransaction().replace(R.id.contendorFragments, new ChaTec()).addToBackStack(null).commit();
                        //getSupportFragmentManager().beginTransaction().replace(R.id.contendorFragments,new ChaTec()).commit();
    
                    }else if(caso==2){
                        //CREO UN INSTANACIA DE LA BASE DE DATOS
                        SQLHelper conn = new SQLHelper(getApplicationContext(),SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);
    
                        //LA ESTABLESCO EN MODO LECTURA
                        SQLiteDatabase db=conn.getReadableDatabase();
    
                        //CREO UN CURSOR PARA RECORRER LOS CAMPOS
                        Cursor cursor = db.rawQuery("SELECT nombre from fotoperfil;", null);
    
                        //CREO VARIABLES PARA GUARDAR LO QUE ME RETORNE EL CURSOR SEGÚN EL TIPO DE VALOR O DATO
                        String nombre_id_foto_perfil="";
    
                        while(cursor.moveToNext()){
                            //GUARDO LOS RESULTADOS EN LAS VARIABLES
                            nombre_id_foto_perfil  = cursor.getString(0);
                            
                        }
    
                        // CREO UNA REFERENCIA DEL STORAGE DE FIREBASE
                        StorageReference storageRef = storage.getReference();
                        
                        //CREO LA REFERENCIA CON RUTA DE LA FOTO QUE ELIMINARÉ CONCATENANDO EL ID (NOMBRE DE LA FOTO EN FIREBASE) DE LA BD
                        StorageReference desertRef = storageRef.child("fotos_perfil/"+nombre_id_foto_perfil);

                        //LLAMO AL MÉTODO QUE ELIMINA LOS ARCHIVOS
                        desertRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                            
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                //EN CASO DE QUE NO SE PUEDA ELIMINAR LA FOTO, SE PONDRÁ UNA POR DEFAULT
                                Toast.makeText(getApplicationContext(),"Hubo un detalle con la red, intenta nuevamente", Toast.LENGTH_SHORT).show();
                                //SE ACTUALIZA FOTO
                                actualizarFoto(Uri.parse("https://i0.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?resize=256%2C256&quality=100&ssl=1"),USUARIO.getCorreo());
                                USUARIO.setFoto(Uri.parse("https://i0.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?resize=256%2C256&quality=100&ssl=1").toString());
                                FragmentManager fm = getFragmentManager();
                                fm.beginTransaction().replace(R.id.contendorFragments, new ChaTec()).addToBackStack(null).commit();
                                
                            }
                        });
        
                        //PONGO LA BD EN MODO ESCRITURA
                        conn.getWritableDatabase();
    
                        //ACTUALIZO EL CAMPO DE LA LA TABLA CON EL NUEVO ID DE LA FOTO ACTUALIZADA
                        db.execSQL("UPDATE fotoperfil SET nombre='"+fotoReferencia.getName()+"';");
                        
                        //SE ACTUALIZA FOTO
                        actualizarFoto(task.getResult(),USUARIO.getCorreo());
                        USUARIO.setFoto(task.getResult().toString());
                        FragmentManager fm = getFragmentManager();
                        fm.beginTransaction().replace(R.id.contendorFragments, new ChaTec()).addToBackStack(null).commit();
                        //getSupportFragmentManager().beginTransaction().replace(R.id.contendorFragments,new ChaTec()).commit();
                        
                        
                    }


            }
        });

        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                double cargado = (taskSnapshot.getBytesTransferred()/1024);
                double total = (taskSnapshot.getTotalByteCount()/1024);
                int cargaActual = (int) cargado;


                progressDialog.setTitle("Subiendo tu foto, aguanta un rato");
                progressDialog.setMessage(((int)progress) + "% subido...");
                progressDialog.setCancelable(false);
                progressDialog.setProgressStyle(2);
                progressDialog.show();


            }
        });


    }

    private void cargarWebService(String correo, Uri urlFoto) {

        //Url del webservice
        String url =     CONSTANTES.URL_PRINCIPAL+ "foto_perfil_insertar.php?correo=" + correo + "&urlfoto=" + urlFoto.toString().replace("%", "%25").replace("&", "%26");

              StringRequest requestt = new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(getApplicationContext(),"Se subió tu foto!",Toast.LENGTH_LONG).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Hubo un problema al intentar subir tu foto",Toast.LENGTH_LONG).show();
                System.out.println("ERROR DEL VOLLEY AL SUBIR FOTO: " +error.toString());
            }
        });

        requestQueue.add(requestt);
    }

    public void buscarFoto(String correo){

        final String url =    CONSTANTES.URL_PRINCIPAL+ "traer_foto_usuario.php?correo="+correo;

        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                JSONArray json = response.optJSONArray("usuariophoto");
                JSONObject jsonObject = null;

                try {
                    jsonObject=json.getJSONObject(0);
                    USUARIO.setFoto(jsonObject.optString("urlfoto"));

                    Uri uriFotoConversion = Uri.parse(USUARIO.getFoto());

                    Glide.with(getApplicationContext()).load(uriFotoConversion).into(imagenPerfil);

                    FragmentManager fm = getFragmentManager();
                    fm.beginTransaction().replace(R.id.contendorFragments, new ChaTec()).addToBackStack(null).commit();
                    // getSupportFragmentManager().beginTransaction().replace(R.id.contendorFragments,new ChaTec()).commit();

                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(),"Problemas al cargar la foto de perfil" + e.toString(),Toast.LENGTH_SHORT).show();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getApplicationContext(),"No hay foto aún",Toast.LENGTH_LONG).show();


                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("SELECCIONA UNA FOTO").setCancelable(false)
                        .setMessage("Necesitas una foto para poder iniciar.")

                        .setPositiveButton("SELECCIONAR FOTO",
                                new DialogInterface.OnClickListener() {
                                    @TargetApi(11)
                                    public void onClick(DialogInterface dialog, int id) {
                                        caso=1;

                                        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                                        i.setType("image/jpeg");
                                        i.putExtra(Intent.EXTRA_LOCAL_ONLY,true);
                                        startActivityForResult(Intent.createChooser(i,"Selecciona una foto"),PHOTO_PERFIL);


                                        dialog.cancel();
                                    }
                                }).show();


            }
        });

        requestQueue.add(jsonObjectRequest);

    }

    public void actualizarFoto(final Uri nuevaUrl, String correoUsuario){
        //Url del webservice
        String url =     CONSTANTES.URL_PRINCIPAL+ "actualizar_foto_perfil.php?correo=" + correoUsuario + "&urlfoto=" + nuevaUrl.toString().replace("%", "%25").replace("&", "%26");

        StringRequest requestt = new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(getApplicationContext(),"Foto actualizada!",Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Se produjo un problema al intentar subir tu foto",Toast.LENGTH_LONG).show();
            }
        });

        requestQueue.add(requestt);

    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getFragmentManager();

        fm.beginTransaction().replace(R.id.contendorFragments, new ChaTec()).addToBackStack(null).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            startActivity(new Intent(MainActivity.this, AcercaDe.class));

            return true;
        } if (id == R.id.sugenrencias) {


            LayoutInflater inflater = (LayoutInflater) getSystemService(Service.LAYOUT_INFLATER_SERVICE);

            final Dialog builder = new Dialog(MainActivity.this);
            builder.requestWindowFeature(Window.FEATURE_NO_TITLE);

            final View dialoglayout = inflater.inflate(R.layout.tarjeta_enviar_correo, null);
            Button btnSend = dialoglayout.findViewById(R.id.btnSend);

             btnSend.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {

                    //obtenemos los datos para el envío del correo
                   EditText etEmail = (EditText)  dialoglayout.findViewById(R.id.etEmail);
                   EditText etSubject = (EditText) dialoglayout. findViewById(R.id.etSubject);
                   EditText etBody = (EditText)  dialoglayout.findViewById(R.id.etBody);
                   CheckBox chkAttachment = (CheckBox) dialoglayout. findViewById(R.id.chkAttachment);

                   //es necesario un intent que levante la actividad deseada
                   Intent itSend = new Intent(android.content.Intent.ACTION_SEND);

                   //vamos a enviar texto plano a menos que el checkbox esté marcado
                   itSend.setType("plain/text");

                   //colocamos los datos para el envío
                   itSend.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{ etEmail.getText().toString()});
                   itSend.putExtra(android.content.Intent.EXTRA_SUBJECT, etSubject.getText().toString());
                   itSend.putExtra(android.content.Intent.EXTRA_TEXT, etBody.getText());

                   //revisamos si el checkbox está marcado enviamos el ícono de la aplicación como adjunto
                   if (chkAttachment.isChecked()) {
                       //colocamos el adjunto en el stream
                       itSend.putExtra(Intent.EXTRA_STREAM, Uri.parse("android.resource://" + getPackageName() + "/" + R.drawable.icoct));

                       //indicamos el tipo de dato
                       itSend.setType("image/png");
                   }

                   //iniciamos la actividad
                   startActivity(itSend);

               }
           });

                    builder.setContentView(dialoglayout);


            builder.show();


            Toast.makeText(getApplicationContext(), "Sugerencia", Toast.LENGTH_SHORT).show();



            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")

    @Override

    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentManager fm = getFragmentManager();
        if (id == R.id.chatec) {

            fm.beginTransaction().replace(R.id.contendorFragments, new ChaTec()).addToBackStack(null).commit();

            Toast.makeText(getApplicationContext(),"ChaTec!", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.vendedores) {

            fm.beginTransaction().replace(R.id.contendorFragments, new Vendedores()).addToBackStack(null).commit();

            Toast.makeText(getApplicationContext(),"Vendedores!", Toast.LENGTH_SHORT).show();

        }else if (id == R.id.calendario) {
    
            fm.beginTransaction().replace(R.id.contendorFragments, new CalendarioITVH()).addToBackStack(null).commit();
    
            Toast.makeText(getApplicationContext(),"Calendario!", Toast.LENGTH_SHORT).show();
    
        } else if (id == R.id.comples) {
            fm.beginTransaction().replace(R.id.contendorFragments, new Complementarias()).addToBackStack(null).commit();

            Toast.makeText(getApplicationContext(),"Complementarias!", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.ingles) {

            fm.beginTransaction().replace(R.id.contendorFragments, new Ingles()).addToBackStack(null).commit();

            Toast.makeText(getApplicationContext(),"Inglés!", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.locaTec) {

            fm.beginTransaction().replace(R.id.contendorFragments, new TipoLocaTec()).addToBackStack(null).commit();

            Toast.makeText(getApplicationContext(),"LocaTec!", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.sws) {

            fm.beginTransaction().replace(R.id.contendorFragments, new SWS()).addToBackStack(null).commit();

            Toast.makeText(getApplicationContext(),"SWS!", Toast.LENGTH_SHORT).show();


        } else if (id == R.id.referencias) {

            fm.beginTransaction().replace(R.id.contendorFragments, new SpeakTec()).addToBackStack(null).commit();

            Toast.makeText(getApplicationContext(),"SpeakTec!", Toast.LENGTH_SHORT).show();


        }else if(id==R.id.avisos){
            fm.beginTransaction().replace(R.id.contendorFragments, new Avisos()).addToBackStack(null).commit();

            Toast.makeText(getApplicationContext(),"Avisos!", Toast.LENGTH_SHORT).show();

        }else if(id==R.id.salir_cerrar){

            String[] despidos = new String[]{"Participa en clases","Piensa en grande y atrévete a fallar","Si no hay esfuerzo, no habrá éxito","Ayuda a los demás","No vivas criticando ni evidiando","Destapa tu potencial","No contamines","Acepta otras ideas","¡Lee un libro!","Agradece a la gente de la limpieza", "Colabora en equipo","Sé humilde","Cuida tus cosas","¡Sé puntual!","¡Nos vemos!", "¡Haz ejercicio!", "Es bueno equivocarse", "¡Suerte en tus exámenes!", "¡Échale ganas!","Respeta el espacio ajeno","No memorices, aprende, comprende", "Quéjate menos y agradece más"};

            int numDespido= (int) (Math.random()*despidos.length);

            Toast.makeText(getApplicationContext(),despidos[numDespido], Toast.LENGTH_LONG).show();
    
    
            finish();
            
			}else if(id==R.id.sie){

            fm.beginTransaction().replace(R.id.contendorFragments, new SIE()).addToBackStack(null).commit();

            Toast.makeText(getApplicationContext(),"SIE!", Toast.LENGTH_SHORT).show();

        }else if(id==R.id.sw_sesion_item){

            swichtSesion.setChecked(!swichtSesion.isChecked());
        }else if(id==R.id.sw_tema_black_item){

           swichtTema.setChecked(!swichtTema.isChecked());

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }



}
