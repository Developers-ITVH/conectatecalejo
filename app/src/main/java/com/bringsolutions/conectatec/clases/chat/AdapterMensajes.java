package com.bringsolutions.conectatec.clases.chat;

import android.app.Dialog;
import android.app.Service;
import android.content.Context;

import android.content.res.ColorStateList;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bringsolutions.conectatec.BD.SQLHelper;
import com.bringsolutions.conectatec.R;
import com.bringsolutions.conectatec.objetos.CONSTANTES;
import com.bringsolutions.conectatec.objetos.Usuario;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.bringsolutions.conectatec.objetos.CONSTANTES.USUARIO;


public class AdapterMensajes extends RecyclerView.Adapter<HolderMensaje> {

    private List<MensajeRecibir> listMensaje = new ArrayList<>();
    private Context c;

    JsonObjectRequest jsonObjectRequest;
    RequestQueue requestQueue;


    String id="" ;
    String nombre="";
    String correo ="";
    String carrera="";
    String tipo ="";
    String telefono="" ;


    public AdapterMensajes(Context c) {
        this.c = c;
    }

    public void addMensaje(MensajeRecibir m){
        listMensaje.add(m);
        notifyItemInserted(listMensaje.size());
    }

    @Override
    public HolderMensaje onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(c).inflate(R.layout.card_view_mensajes,parent,false);


        return new HolderMensaje(v);
    }

    @Override
    public void onBindViewHolder(final HolderMensaje holder, final int position) {


        holder.getNombre().setText(listMensaje.get(position).getNombre());

        holder.getMensaje().setText(listMensaje.get(position).getMensaje());

        if(listMensaje.get(position).getType_mensaje().equals("2")){
            holder.getFotoMensaje().setVisibility(View.VISIBLE);
            holder.getMensaje().setVisibility(View.VISIBLE);
            holder.getTarjetaMensa().setCardBackgroundColor(Color.TRANSPARENT);
            holder.getTarjetaMensa().setElevation(0);
            holder.getNombre().setTextColor(Color.BLACK);
            holder.getHora().setTextColor(Color.BLACK);
            holder.getMensaje().setTextColor(Color.BLACK);

            int ancho = 150;
            int alto = 150;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ancho, alto);
            holder.getFotoMensaje().setLayoutParams(params);

            Glide.with(c).load(listMensaje.get(position).getUrlFoto()).into(holder.getFotoMensaje());

            if(USUARIO.getTipo().equals("2")){
                holder.getTarjetaMensa().setCardBackgroundColor(Color.parseColor("#A9DFBF"));

            }

            holder.getFotoMensaje().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LayoutInflater inflater = (LayoutInflater) c.getSystemService(Service.LAYOUT_INFLATER_SERVICE);

                    final Dialog builder = new Dialog(c);
                    builder.requestWindowFeature(Window.FEATURE_NO_TITLE);

                    View dialoglayout = inflater.inflate(R.layout.imagen_dialog, null);
                    ImageView imagenInflada = dialoglayout.findViewById(R.id.imagenDialog);

                    builder.setContentView(dialoglayout);

                    Glide.with(c).load(listMensaje.get(position).getUrlFoto()).into(imagenInflada);

                    builder.show();
                }
            });


        }else if(listMensaje.get(position).getType_mensaje().equals("1")){
            holder.getFotoMensaje().setVisibility(View.GONE);
            holder.getMensaje().setVisibility(View.VISIBLE);
        }
        if(listMensaje.get(position).getFotoPerfil().isEmpty()){


            holder.getFotoMensajePerfil().setImageResource(R.mipmap.ic_launcher);
        }else{
            Glide.with(c).load(listMensaje.get(position).getFotoPerfil()).into(holder.getFotoMensajePerfil());
        }


        Long codigoHora = listMensaje.get(position).getHora();
        Date d = new Date(codigoHora);
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss a");//a pm o am

        holder.getHora().setText(sdf.format(d));

        holder.getTarjetaMensa().setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                requestQueue = Volley.newRequestQueue(c);


                String url =    CONSTANTES.URL_PRINCIPAL+ "buscar_u_xcorreo.php?correo="+listMensaje.get(position).getCorreo();

                jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        JSONArray json = response.optJSONArray("usuario");
                        JSONObject jsonObject = null;

                        try {
                            //CARGO UN OBJETO DE TIPO USUARIO Y SETEO LOS DATOS CONSUMIDOS POR EL WEBSERVICE
                            jsonObject=json.getJSONObject(0);

                             id = jsonObject.optString("idusuario");
                             nombre=jsonObject.optString("nombre");
                             correo = jsonObject.optString("correo");
                             carrera=jsonObject.optString("carrera");
                             tipo = jsonObject.optString("tipo");
                             telefono = jsonObject.optString("telefono");



                            LayoutInflater inflater = (LayoutInflater) c.getSystemService(Service.LAYOUT_INFLATER_SERVICE);

                            final Dialog builder = new Dialog(c);
                            builder.requestWindowFeature(Window.FEATURE_NO_TITLE);

                            View dialoglayout = inflater.inflate(R.layout.tarjeta_opciones_mensaje, null);
                            ImageView imagenInflada = dialoglayout.findViewById(R.id.fotitoUsuarioChat);
                            TextView nombrecitoUsuario = dialoglayout.findViewById(R.id.nombreUsuarioChat);
                            Button btnReportar =  dialoglayout.findViewById(R.id.btnReportarUsuarioChat);


                            CardView tarjetitaMensaje  =  dialoglayout.findViewById(R.id.tarjetitaChatUsuario);

                                 //CREO UN INSTANACIA DE LA BASE DE DATOS
                            SQLHelper conn = new SQLHelper(c,SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);

                            //LA ESTABLESCO EN MODO LECTURA
                            SQLiteDatabase db=conn.getReadableDatabase();

                            //CREO UN CURSOR PARA RECORRER LOS CAMPOS
                            Cursor cursor = db.rawQuery("SELECT estado from tema;", null);

                            //CREO VARIABLES PARA GUARDAR LO QUE ME RETORNE EL CURSOR SEGÚN EL TIPO DE VALOR O DATO
                            String estado="";

                            while(cursor.moveToNext()){

                                //GUARDO LOS RESULTADOS EN LAS VARIABLES
                                estado  = cursor.getString(0);

                                if(estado.equals("true")){

                                    //TEMA BLACk
                                    tarjetitaMensaje.setCardBackgroundColor(Color.parseColor("#1F1E1E"));
                                    nombrecitoUsuario.setTextColor(Color.parseColor("#F4D121"));

                                }else{


                                }


                            }

                            builder.setContentView(dialoglayout);

                            nombrecitoUsuario.setText(nombre);
                            btnReportar.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    //Url del webservice
                                    String url =     CONSTANTES.URL_PRINCIPAL+"actualizar_num_reportes.php?correo="+correo;

                                    StringRequest requestt = new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            Toast.makeText(c,"USUARIO REPORTADO!",Toast.LENGTH_LONG).show();



                                        }
                                    }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Toast.makeText(c,"HUBO UN PROBLEMA!",Toast.LENGTH_LONG).show();

                                        }
                                    });

                                    requestQueue.add(requestt);




                                }
                            });

                            Glide.with(c).load(Uri.parse(listMensaje.get(position).getFotoPerfil())).into(imagenInflada);

                            builder.show();





                        } catch (JSONException e) {

                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(c, "Se produjo un error al enviar el reporte." , Toast.LENGTH_SHORT).show();


                    }
                });



                requestQueue.add(jsonObjectRequest);


                return false;
            }
        });


    }



    @Override
    public int getItemCount() {
        return listMensaje.size();
    }

}
