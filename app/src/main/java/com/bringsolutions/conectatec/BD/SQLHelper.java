package com.bringsolutions.conectatec.BD;

import android.content.Context;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLHelper extends SQLiteOpenHelper  {

    String tablaRecordarPass = "CREATE TABLE recordarpass(idrecordar INTEGER PRIMARY KEY AUTOINCREMENT, estado TEXT, correo TEXT, pass, TEXT)";
    String tablaTaps = "CREATE TABLE uso (idUso INTEGER PRIMARY KEY AUTOINCREMENT,estado TEXT)";
    String tablaTema = "CREATE TABLE tema (idTema INTEGER PRIMARY KEY AUTOINCREMENT,estado TEXT)";
    String contadorVendedor = "CREATE TABLE contador (idContador INTEGER PRIMARY KEY AUTOINCREMENT,cuenta TEXT, cuentaspeak TEXT)";
    
    String registroIDFotoPerfil = "CREATE TABLE fotoperfil (idFoto INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT)";
    String registroIDFotosProductos = "CREATE TABLE fproductos (idFotoPro INTEGER PRIMARY KEY AUTOINCREMENT, f1 TEXT, f2 TEXT, f3 TEXT)";
    
    
    //  String lugarSelecionado = "CREATE TABLE lugar (idlugar INTEGER PRIMARY KEY AUTOINCREMENT,nombre TEXT, latitud TEXT, logitud TEXT)";

    public static final String BASE_DATOS_NOMBRE = "recordar";

    public static final int BASE_DATOS_VERSION = 1;

    public SQLHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(tablaTaps);
        sqLiteDatabase.execSQL(tablaTema);
        sqLiteDatabase.execSQL(tablaRecordarPass);
        sqLiteDatabase.execSQL(contadorVendedor);
        sqLiteDatabase.execSQL(registroIDFotoPerfil);
        sqLiteDatabase.execSQL(registroIDFotosProductos);
    
        sqLiteDatabase.execSQL("INSERT INTO recordarpass (estado ,correo, pass) values ('false','sincorreo','sinpass')");
        sqLiteDatabase.execSQL( "INSERT INTO tema(estado) values ('false')");
        sqLiteDatabase.execSQL( "INSERT INTO contador(cuenta,cuentaspeak) values ('1','1')");
    
        sqLiteDatabase.execSQL( "INSERT INTO fotoperfil(nombre) values ('SINID')");
        sqLiteDatabase.execSQL( "INSERT INTO fproductos(f1,f2,f3) values ('vacio1','vacio2','vacio3')");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS recordarpass");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS uso");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS tema");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS contador");
    
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS fotoperfil");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS fproductos");


    }
}
